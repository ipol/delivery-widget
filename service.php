<?php

define ('FML_Authorization', 'Bearer 83fa5a432ae55c253d0e60dbfa716723'); // Сюда необходимо поставить токен!


header('Access-Control-Allow-Origin: *');
error_reporting(0);

if ($_GET['method'] == 'pvzList') {
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => 'https://ecom.fmlogistic.com/services/merchant/getAddress/all',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
                "postcode": "' . $_POST['postcode'] . '",
                "cityName": "' . $_POST['cityName'] . '"
            }',
        CURLOPT_HTTPHEADER => [
            'Authorization: '.FML_Authorization,
            'Content-Type: application/json'
        ],
    ]);

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;
}


if ($_GET['method'] == 'shippingMethods') {

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => 'https://ecom.fmlogistic.com/openapi/shipping-methods',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{'
            . '"address": "' . $_POST['address'] . '",'
            .'"weight": ' . $_POST['weight'] . ''
        . '}',
        CURLOPT_HTTPHEADER => [
            'Authorization: '.FML_Authorization,
            'Content-Type: application/json'
        ],
    ]);

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;
}