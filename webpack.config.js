const path = require('path');

module.exports = {
    entry: './lib/fmlogisticwidget.js',
    output: {
        path: path.resolve(__dirname, "release"),
        filename: './fmlogisticwidget.min.js',
        library: 'fmlogisticwidget'
    }
};