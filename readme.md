# Официальный виджет FM-Logistics

*FM-Logistics — это одна из крупнейших логистических и складских компаний*

Виджет позволит вам добавить на свой сайт функционал для расчета стоимости доставки и отображения пунктов выдачи на карте.
Для работы виджета вам понадобиться **API ключ от FM-Logistics** и **ключ доступа к Яндекс.Картам**.

Чтобы получить ключ доступа Яндекс.карт зайдите на страницу Кабинета Разработчика: [https://developer.tech.yandex.ru/services/](https://developer.tech.yandex.ru/services/) и нажмите кнопку Получить ключ. Во всплывающем окне выберите сервис «JavaScript API и HTTP Геокодер».

## Установка

1. Для начала определитесь в каком разделе сайта у вас будут находится скрипты виджета и закачайте туда 2 скрипта из данного репозитория: `service.php` и `release/fmlogisticwidget.min.js`
Примеры ниже указаны из расчета что оба файла у вас загружены просто в корень сайта.   
Файлы index.php и popup.php - это демонстрационные примеры подключения виджета. Остальные файлы вспомогательные для сборки минимизированной версии из исходника.


2. В файле service.php укажите вместо `Bearer 83fa5a432ae55c253d0e60dbfa716723` ваш токен доступа к API FML


3. На странице сайта где должен располагаться виджет в блоке `<head>` добавьте тег настройки отображения мобильной версии:
```html
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
```
* \*Проверьте что наличие этого тега не нарушает работу вашей текущей моб.версии сайта. И что тег viewport не присутствовал уже в коде страницы. 

4. Добавьте в блоке `<head>`, либо в любом другом месте перед вызовом самого виджета код подключения скрипта виджета:

```html
<script type="text/javascript" src="/fmlogisticwidget.min.js"></script>
```
Обратите внимание, что в src должен быть корректный путь к виджету в зависимости от раздела куда вы разместили файл, например “/myFmWidget/fmlogisticwidget.min.js”

5. Определите на странице тег в котором будет инициализироваться виджет, например
```html
<div id="for_fmlogisticwidget"></div>
```
6. Добавьте на страницу код инициализации виджета:
 
```html
<script type="text/javascript">
    new FMLogisticWidget({
        service_path: '/service.php',  //тут должен быть путь к месту где вы разместили файл service.php, например  “/myFmWidget/service.php”
        link: 'for_fmlogisticwidget', //id тега в котором должен инициализироваться виджет установленного в п.5
        cargo: {
            weight: 10,// вес заказа в килограммах для которого рассчитывается доставка
            max_weight : 3,// снова вес заказа в кг. 
            max_size : 10,//Максимальная длинна или высота груза в см.
        },
        yandex_map_api_key: '76211b49-d943-4b54-bc7a-86949d716e19', //Код яндекс.карт
        //Необязательные параметры
        //default_city: 'Россия, Москва ', // Город по умолчанию для которого откроется виджет
        //searchArea: [[54.496848063378806, 55.29955631249997],[54.97205624926723, 56.617915687500016]], // Поиск только по заданной области (Квадрат области по координатам в яндекс картах)
        //only_delivery_type: 'Pvz', // Pvz или Courier // возможность задать ограничения виджету для работы только с ПВЗ или только с курьерскими вариантами
        //popup_mode: true, // режим popup добавляет кнопку закрытия
        //only_info: true, // Только отображение информации без кнопок выбора
        onSelectCourierItem: function (ItemCourier) {
            console.log(ItemCourier.getData())
        },
        onSelectPvzItem: function (ItemPvz){ //Событие срабатывающее после выбора варианта ПВЗ. 
            console.log(ItemPvz.getData())
        },
        onAjaxGetShippingMethods: function (Result) { //Событие после получения вариантов доставки (тут можно переопределить стоимость доставки меняя массив Result, добавив собственные правила бесплатной доставки и т.д.)
            console.log(Result);
        },
        onAjaxGetPickPoints: function (Result) { //Событие после получения точек ПВЗ (тут можно добавить собственные программные фильтры на получаемые точки ПВЗ)
            console.log(Result);
        }
    });
</script>
```

Пример как можно переопределить стоимость доставки на фиксированную, добавив в метод `onAjaxGetShippingMethods:` следующий код:
```javascript
     function callbackOnAjaxGetShippingMethods(Result) {	
        if (!Result.shipping_methods) {
            return;
        }
        Result.shipping_methods.forEach(function(item) {
            item.price = 10; // вся стоимость доставки станет равной 10 рублей.
        });
    }
```
----

Виджет должен появиться на странице.
Пример: https://ipol.ru/webService/fm-logistic/widget/

Если вы хотите открывать виджет во всплыв.окне, то можете добавить на своей стороне кнопку открытия виджета, подложку и код вызывающий виджет и управляющий видимостью подложки. Пример подключения виджета в виде всплыв.окна смотрите в файле popup.php
Демо-пример можно увидеть по ссылке: https://ipol.ru/webService/fm-logistic/widget/popup.php


Также виджет имеет методы `destroy` и `reinitialize`
Пример: 
есть разные параметры
```javascript
    let params = {...};
    let anotherParams = {...};
```
Создаём объект виджета
```javascript
let WidgetObject = new FMLogisticWidget(params);
```
мы можем проинициализировать виджет c другими параметрами
```javascript
WidgetObject.reinitialize(anotherParams);
```
пересоздать виджет
```javascript
WidgetObject.destroy()
WidgetObject = new FMLogisticWidget(anotherParams);
```
Также можно не использовать метод `destroy` если создавать в том-же элементе виджет сам проверит наличие карты и Html структуры и пересоздаст с новыми параметрами
```javascript
WidgetObject = new FMLogisticWidget(anotherParams);
```


Используйте `release/fmlogisticwidget.min.js` вместо `src/fmlogisticwidget.js` он имеет поддержку более старых версий браузеров
`src/fmlogisticwidget.js` необходим только для ознакомления и разработки.

В случае необходимости работы с исходниками после внесения изменений в `src/fmlogisticwidget.js` пересоберите проект.

Для сборки в директории проекта выполните сконфигурированную команду
```shell
npm run build
```

Для работы понадобится `npm` и будет необходимо подтянуть пакеты описанные в `package.json`, выполнив команду в директории проекта
```shell
npm install
```




