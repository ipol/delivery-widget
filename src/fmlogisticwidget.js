(function(window) {
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.SuggestView
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.vow
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.map
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.Map
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.Map().container
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.Map().container._element
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.control
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.control.ZoomControl
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.templateLayoutFactory
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.templateLayoutFactory.createClass
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.Map.setCenter
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.Map.geoObjects
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.Map.setZoom
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.Placemark
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.geolocation
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.geocode
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.control
	 */
	/**
	 * @type {Function}
	 * @name FmlYmapNamespace.control.SearchControl
	 */
	/**
	 * @type {Object}
	 * @name FmlYmapNamespace.control.SearchControl.events
	 */


	/**
	 * @type {{ButtonBack: string, Filter: string, Payment_all: string, Payment_cash: string, DetailBuck: string, Point: string, Payment_card: string, FilterActive: string}}
	 */
	const SvgAssets = {
		Point: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><path d="M0 1H5.33333V6.33333H0V1ZM8 2.33333V5H24V2.33333H8ZM0 9H5.33333V14.3333H0V9ZM8 10.3333V13H24V10.3333H8ZM0 17H5.33333V22.3333H0V17ZM8 18.3333V21H24V18.3333H8Z" fill="#0073BC"/></svg>',
		Filter: '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none"><rect width="32" height="32" rx="16" fill="#0073BC"/><path d="M25.993 11.9407C25.99 11.8687 25.983 11.7957 25.964 11.7267C25.957 11.7017 25.943 11.6778 25.934 11.6528C25.913 11.5958 25.894 11.5398 25.864 11.4878C25.848 11.4608 25.826 11.4387 25.807 11.4127C25.775 11.3677 25.744 11.3217 25.705 11.2827C25.682 11.2607 25.652 11.2427 25.627 11.2217C25.588 11.1897 25.552 11.1547 25.507 11.1277C25.503 11.1247 25.498 11.1247 25.493 11.1217C25.49 11.1207 25.488 11.1177 25.485 11.1157L16.506 6.12575C16.205 5.95775 15.839 5.95875 15.536 6.12475L6.515 11.1147C6.512 11.1177 6.509 11.1217 6.504 11.1247C6.501 11.1257 6.497 11.1267 6.494 11.1287C6.459 11.1487 6.433 11.1777 6.4 11.2017C6.364 11.2287 6.326 11.2528 6.294 11.2838C6.264 11.3148 6.241 11.3507 6.215 11.3857C6.188 11.4207 6.158 11.4517 6.136 11.4897C6.11 11.5327 6.096 11.5817 6.077 11.6287C6.063 11.6617 6.045 11.6927 6.036 11.7287C6.017 11.7957 6.011 11.8677 6.007 11.9387C6.006 11.9557 6 11.9707 6 11.9887V19.9908C6 20.3538 6.197 20.6888 6.515 20.8648L15.493 25.8517L15.494 25.8527C15.495 25.8527 15.495 25.8528 15.496 25.8538L15.516 25.8647C15.559 25.8887 15.606 25.9017 15.651 25.9187C15.683 25.9317 15.714 25.9487 15.748 25.9577C15.829 25.9787 15.915 25.9907 16 25.9907C16.085 25.9907 16.171 25.9787 16.254 25.9577C16.287 25.9487 16.318 25.9317 16.351 25.9187C16.396 25.9017 16.443 25.8897 16.486 25.8647L16.506 25.8538C16.507 25.8528 16.507 25.8527 16.508 25.8527L16.509 25.8517L25.487 20.8648C25.803 20.6888 26 20.3538 26 19.9908V11.9887C26 11.9717 25.994 11.9577 25.993 11.9407ZM15.972 15.8628L9.058 11.9957L11.82 10.4678L18.654 14.3728L15.972 15.8628ZM16.02 8.14375L22.941 11.9907L20.697 13.2377L13.867 9.33475L16.02 8.14375ZM17 23.2917L17.002 17.6128L20 15.9348V18.9908L22 17.9908V14.8158L24 13.6968V19.4018L17 23.2917Z" fill="white"/></svg>',
		FilterActive: '<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.4241 6.63315C10.4241 6.45225 10.4844 6.27134 10.4844 6.03014C10.4844 5.78893 10.4844 5.60803 10.4241 5.42713L11.6904 4.402C11.811 4.2814 11.811 4.1608 11.7507 4.04019L10.5447 1.92964C10.4844 1.86934 10.3638 1.80904 10.1829 1.86934L8.67535 2.47236C8.37384 2.23115 8.01203 2.05025 7.65022 1.86934L7.40902 0.301507C7.46932 0.120603 7.28839 0 7.16781 0H4.75575C4.63513 0 4.45425 0.120603 4.45425 0.241206L4.21304 1.86934C3.85123 1.98995 3.54973 2.23115 3.18792 2.47236L1.74068 1.86934C1.55978 1.80904 1.43918 1.86934 1.31855 1.98995L0.112524 4.10049C0.0522223 4.1608 0.112524 4.3417 0.233127 4.4623L1.49946 5.42713C1.49946 5.60803 1.43915 5.78893 1.43915 6.03014C1.43915 6.27134 1.43915 6.45225 1.49946 6.63315L0.233127 7.65828C0.112524 7.7789 0.112524 7.89948 0.172825 8.02011L1.37885 10.1307C1.43915 10.191 1.55973 10.2513 1.74066 10.191L3.2482 9.58794C3.5497 9.82915 3.91151 10.0101 4.27332 10.191L4.51453 11.7588C4.51453 11.8794 4.63513 12 4.81603 12H7.22809C7.34871 12 7.52959 11.8794 7.52959 11.7588L7.7708 10.191C8.13261 10.0101 8.49442 9.82915 8.79592 9.58794L10.3035 10.191C10.4241 10.2513 10.605 10.191 10.6653 10.0703L11.8713 7.95978C11.9316 7.83918 11.9316 7.65828 11.811 7.59798L10.4241 6.63315ZM5.96178 8.14069C4.81606 8.14069 3.85123 7.17587 3.85123 6.03014C3.85123 4.88441 4.81606 3.91959 5.96178 3.91959C7.10751 3.91959 8.07233 4.88441 8.07233 6.03014C8.07233 7.17587 7.10751 8.14069 5.96178 8.14069Z" fill="white"></path></svg>',
		Payment_cash: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="32" viewBox="0 0 24 32" fill="none"><path d="M14.9995 22.4981V30.3165C15.0595 30.7664 14.9095 31.2464 14.5646 31.5613C13.9797 32.1462 13.0348 32.1462 12.4499 31.5613L9.43542 28.5467C9.09048 28.2018 8.94051 27.7368 9.00049 27.3019V22.4981H8.9555L0.316931 16.4297C-0.192984 15.7848 -0.0730041 0.839884 0.571889 0.329954C0.856842 0.119983 1.17179 0 1.50174 0H22.4983C22.8282 0 23.1432 0.119983 23.4281 0.329954C24.073 0.839884 24.193 15.7848 23.6831 16.4297L15.0445 22.4981H14.9995Z" fill="#0073BC"/><path fill-rule="evenodd" clip-rule="evenodd" d="M15.4793 7.98557C15.1735 7.80802 14.6803 7.80802 14.3745 7.98557C14.0687 8.16313 14.072 8.4492 14.3778 8.62675C14.6836 8.80431 15.1801 8.80431 15.4826 8.62675C15.7884 8.4492 15.7851 8.16313 15.4793 7.98557ZM9.65605 4.62185C9.34697 4.44429 8.85375 4.44429 8.54796 4.62185C8.24217 4.79941 8.24546 5.08547 8.55454 5.26303C8.86033 5.44059 9.35683 5.44059 9.65934 5.26303C9.96513 5.08547 9.96184 4.79941 9.65605 4.62185ZM13.1316 11.0271L4.40824 5.99299C3.90516 5.70035 3.90188 5.22686 4.40167 4.93751L9.07406 2.21825C9.57714 1.9289 10.3959 1.92561 10.899 2.21825L19.6223 7.25562C20.1254 7.54497 20.1254 8.01846 19.6256 8.3111L14.9532 11.0271C14.4534 11.3197 13.6346 11.3197 13.1316 11.0271ZM6.12463 4.83886C6.18711 4.86517 6.24629 4.89147 6.30219 4.92435C6.81184 5.22028 6.81513 5.70035 6.30548 5.99299C6.24958 6.02587 6.18711 6.05546 6.12134 6.08177L12.8948 9.9946C12.9277 9.9683 12.9606 9.94528 13 9.92226C13.5064 9.62634 14.3317 9.62634 14.8447 9.92226C14.9236 9.9683 14.9893 10.0176 15.0452 10.0702L17.8631 8.43276C17.7711 8.39987 17.6856 8.36371 17.6067 8.31767C17.0937 8.02174 17.0904 7.54497 17.6001 7.24904C17.679 7.20301 17.7612 7.16684 17.8533 7.13396L11.0601 3.21126C11.0173 3.25071 10.968 3.28688 10.9088 3.31976C10.4024 3.6124 9.57385 3.61569 9.06419 3.31976C9.00829 3.28688 8.95897 3.254 8.91623 3.21783L6.12463 4.83886ZM4.96064 7.02545C4.74692 6.9005 4.40167 6.88406 4.18137 6.99915C3.94462 7.1208 3.94133 7.33124 4.16164 7.46277L13.3026 12.7369L13.4308 12.8125L13.6149 12.9177C13.8714 13.0657 14.289 13.0657 14.5455 12.9177L19.7834 9.87294C19.9971 9.748 19.9971 9.54742 19.7801 9.42247C19.5664 9.29753 19.2178 9.29753 19.0041 9.42247L14.0753 12.2864L4.96064 7.02545ZM14.5553 14.5651L19.7933 11.5203C20.007 11.3953 20.0037 11.1948 19.79 11.0698C19.573 10.9449 19.2244 10.9449 19.0107 11.0698L14.0851 13.9305L4.97051 8.6695C4.75678 8.54455 4.41153 8.52811 4.19123 8.64319C3.95449 8.76814 3.94791 8.97858 4.1715 9.10682L13.3091 14.3842L13.4407 14.4598L13.6248 14.5651C13.8813 14.713 14.2988 14.713 14.5553 14.5651ZM4.96064 10.3168C4.74692 10.1919 4.40167 10.1754 4.18137 10.2905C3.94462 10.4155 3.94133 10.6259 4.16164 10.7542L13.3026 16.0283L13.4308 16.1039L13.6149 16.2091C13.8714 16.3571 14.289 16.3571 14.5455 16.2091L19.7834 13.1643C19.9971 13.0394 19.9971 12.8388 19.7801 12.7139C19.5664 12.5889 19.2178 12.5889 19.0041 12.7139L14.0753 15.5811L4.96064 10.3168ZM14.5553 17.8564L19.7933 14.8117C20.007 14.6867 20.0037 14.4861 19.7933 14.3579C19.5762 14.233 19.2277 14.233 19.014 14.3579L14.0851 17.2218L4.97051 11.9609C4.75678 11.8359 4.41153 11.8195 4.19123 11.9346C3.95449 12.0595 3.94791 12.27 4.1715 12.3982L13.3091 17.6756L13.4407 17.7512L13.6248 17.8564C13.8813 18.0044 14.2988 18.0044 14.5553 17.8564Z" fill="white"/><path d="M10.0243 7.78171C11.1291 8.4196 12.9178 8.4196 14.0161 7.78171C15.1176 7.14382 15.1077 6.10807 14.0029 5.47018C12.8981 4.83229 11.1094 4.83229 10.0112 5.47018C8.91294 6.10807 8.91952 7.14053 10.0243 7.78171Z" fill="white"/></svg>',
		Payment_card: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="32" viewBox="0 0 24 32" fill="none"><path d="M14.9995 22.4981V30.3165C15.0595 30.7664 14.9095 31.2464 14.5646 31.5613C13.9797 32.1462 13.0348 32.1462 12.4499 31.5613L9.43542 28.5467C9.09048 28.2018 8.94051 27.7368 9.00049 27.3019V22.4981H8.9555L0.316931 16.4297C-0.192984 15.7848 -0.0730041 0.839884 0.571889 0.329954C0.856842 0.119983 1.17179 0 1.50174 0H22.4983C22.8282 0 23.1432 0.119983 23.4281 0.329954C24.073 0.839884 24.193 15.7848 23.6831 16.4297L15.0445 22.4981H14.9995Z" fill="#0073BC"/><path d="M19.4558 3.43537C19.3298 3.30111 19.1782 3.19343 19.01 3.11865C18.8418 3.04388 18.6603 3.00354 18.4762 3H5.41497C5.23041 3.00061 5.048 3.03963 4.87935 3.11459C4.7107 3.18954 4.5595 3.29879 4.43537 3.43537C4.18336 3.70308 4.02983 4.04851 4 4.41497V14.4286C4.00061 14.6131 4.03963 14.7955 4.11459 14.9642C4.18954 15.1328 4.29879 15.284 4.43537 15.4082C4.56135 15.5424 4.71292 15.6501 4.88116 15.7249C5.04939 15.7997 5.23089 15.84 5.41497 15.8435H18.585C18.7696 15.8429 18.952 15.8039 19.1207 15.729C19.2893 15.654 19.4405 15.5447 19.5646 15.4082C19.6989 15.2822 19.8066 15.1306 19.8813 14.9624C19.9561 14.7941 19.9965 14.6126 20 14.4286V4.41497C19.8851 4.05568 19.7001 3.72273 19.4558 3.43537ZM18.8027 14.4286C18.8084 14.4716 18.801 14.5153 18.7816 14.5541C18.7622 14.5929 18.7317 14.625 18.6939 14.6463C18.6352 14.7054 18.5587 14.7437 18.4762 14.7551H5.41497C5.37196 14.7608 5.32825 14.7534 5.28945 14.734C5.25066 14.7146 5.21855 14.6841 5.19728 14.6463C5.13811 14.5876 5.09987 14.5111 5.08844 14.4286V9.42177H18.8027V14.4286ZM18.8027 6.26531H5.08844V4.41497C5.08278 4.37196 5.09012 4.32825 5.10952 4.28945C5.12891 4.25066 5.15948 4.21855 5.19728 4.19728C5.25595 4.13811 5.33243 4.09987 5.41497 4.08844H18.585C18.628 4.08278 18.6718 4.09012 18.7105 4.10952C18.7493 4.12891 18.7814 4.15948 18.8027 4.19728C18.8619 4.25595 18.9001 4.33243 18.9116 4.41497L18.8027 6.26531Z" fill="white"/><path d="M16.8435 11.7075H14.8844V13.5578H16.8435V11.7075Z" fill="white"/></svg>',
		Payment_all: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="32" viewBox="0 0 24 32" fill="none"><path d="M14.9995 22.4981V30.3165C15.0595 30.7664 14.9095 31.2464 14.5646 31.5613C13.9797 32.1462 13.0348 32.1462 12.4499 31.5613L9.43542 28.5467C9.09048 28.2018 8.94051 27.7368 9.00049 27.3019V22.4981H8.9555L0.316931 16.4297C-0.192984 15.7848 -0.0730041 0.839884 0.571889 0.329954C0.856842 0.119983 1.17179 0 1.50174 0H22.4983C22.8282 0 23.1432 0.119983 23.4281 0.329954C24.073 0.839884 24.193 15.7848 23.6831 16.4297L15.0445 22.4981H14.9995Z" fill="#0073BC"/><path d="M15.5853 4.33224H8.41796C8.04898 4.33224 7.74857 4.0351 7.75184 3.66612C7.75184 3.29714 8.04898 3 8.41796 3H15.5853C15.9543 3 16.2514 3.29714 16.2514 3.66612C16.2514 4.0351 15.9543 4.33224 15.5853 4.33224Z" fill="white"/><path d="M6.41633 5.41633H17.5837C17.9527 5.41633 18.2498 5.71673 18.2498 6.08245C18.2498 6.45143 17.9527 6.74857 17.5837 6.74857H6.41633C6.04735 6.74857 5.7502 6.45143 5.7502 6.08245C5.7502 5.71347 6.04735 5.41633 6.41633 5.41633Z" fill="white"/><path d="M12.449 12.2082H11.2204V10.1347H12.5102C12.8476 10.1429 13.1116 10.2435 13.302 10.4367C13.4952 10.6299 13.5918 10.8789 13.5918 11.1837C13.5918 11.5238 13.4939 11.7796 13.298 11.951C13.102 12.1224 12.819 12.2082 12.449 12.2082Z" fill="white"/><path fill-rule="evenodd" clip-rule="evenodd" d="M4.66612 7.78694H19.3339C19.7029 7.78694 20 8.08408 20 8.45306V15.9176C20 16.2865 19.7029 16.5837 19.3339 16.5837H4.66612C4.29714 16.5837 4 16.2865 4 15.9176V8.45306C4 8.08408 4.29714 7.78694 4.66612 7.78694ZM11.2204 14.302H12.5347V13.4735H11.2204V13.0367H12.5102C13.1741 13.0286 13.6925 12.8612 14.0653 12.5347C14.4381 12.2054 14.6245 11.7524 14.6245 11.1755C14.6245 10.6095 14.4286 10.1565 14.0367 9.81633C13.6449 9.47347 13.1211 9.30204 12.4653 9.30204H10.1918V12.2082H9.3551V13.0367H10.1918V13.4735H9.3551V14.302H10.1918V15.2449H11.2204V14.302Z" fill="white"/></svg>',
		ButtonBack: '<svg width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.828 5.95L6.364 8.486L4.95 9.9L0 4.95L4.95 0L6.364 1.414L3.828 3.95H11C13.1217 3.95 15.1566 4.79286 16.6569 6.29315C18.1571 7.79344 19 9.82827 19 11.95C19 14.0717 18.1571 16.1066 16.6569 17.6069C15.1566 19.1071 13.1217 19.95 11 19.95H2V17.95H11C12.5913 17.95 14.1174 17.3179 15.2426 16.1926C16.3679 15.0674 17 13.5413 17 11.95C17 10.3587 16.3679 8.83258 15.2426 7.70736C14.1174 6.58214 12.5913 5.95 11 5.95H3.828Z" fill="#0073BC"></path></svg>',
		DetailBuck: '<svg width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path class="path" d="M12.35 17.6333L4.71667 10L12.35 2.35L10 0L0 10L10 20L12.35 17.6333Z"></path></svg>',
	};
	/**
	 * @type {{PlaceMarksStyles: string, BaseWidgetStyles: string, MobileStyles: string, PopupStyles: string}}
	 */
	const WidgetStyleAssets = {
		BaseWidgetStyles: '@import url(https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap);.fml-progress_animate {animation: 2000ms linear 0s 1 normal none running loading;}.fml-hidden{opacity:0}.fml-filter-loader{position:absolute;bottom:0;height:3px;background-color:red;width:0%;z-index:10}.fml-widget__panel-content__default_logo{content:url(\'data:image/svg+xml; utf8, <svg xmlns="http://www.w3.org/2000/svg" width="34" height="24" viewBox="0 0 34 24" fill="none"><path d="M28.9999 5.99996H24.4999V0H3.50002C1.84247 0 0.5 1.34247 0.5 3.00002V19.5H3.50002C3.50002 21.9825 5.51749 24 8.00001 24C10.4825 24 12.5 21.9825 12.5 19.5H21.5C21.5 21.9825 23.5175 24 26 24C28.4825 24 30.5 21.9825 30.5 19.5H33.5V12L28.9999 5.99996ZM8.00001 21.7499C6.75499 21.7499 5.75001 20.745 5.75001 19.4999C5.75001 18.2549 6.75499 17.2499 8.00001 17.2499C9.24503 17.2499 10.25 18.2549 10.25 19.4999C10.25 20.745 9.24496 21.7499 8.00001 21.7499ZM26 21.7499C24.755 21.7499 23.75 20.745 23.75 19.4999C23.75 18.2549 24.755 17.2499 26 17.2499C27.245 17.2499 28.25 18.2549 28.25 19.4999C28.25 20.745 27.2449 21.7499 26 21.7499ZM24.4999 12V8.25003H28.2499L31.1974 12H24.4999Z" fill="%23B4BAC6"/></svg>\')!important;width:100%;height:72px;max-width:100%;max-height:100%;vertical-align:middle}.fml-loader{position:absolute;width:100%;height:100%;z-index:3;background:rgba(119,119,119,.2)}.fml-preloader{position:absolute;width:40px;height:40px;left:calc(50% - 20px);top:calc(50% - 20px);background:#0073bc;transform:rotate(45deg);animation:preloader 2s linear infinite}.fml-loadBar{position:absolute;width:200px;height:2px;left:calc(50% - 100px);top:calc(50% + 60px);background:rgba(119,119,119,.2)}.fml-progress{position:relative;width:0%;height:inherit;background:#0073bc}@keyframes loading{0%{width:0%}100%{width:100%}}@keyframes preloader{0%,100%{transform:rotate(45deg)}60%{transform:rotate(405deg)}}.fml-widget__map .ymaps-2-1-79-searchbox__normal-layout{width:398px}@media only screen and (max-width:767px){.fml-widget__map .ymaps-2-1-79-searchbox__normal-layout{width:338px}}.fml-widget__map .ymaps-2-1-79-searchbox-input__input{height:36px;line-height:36px;font-size:15px;font-family:Roboto,sans-serif;font-weight:400;-webkit-border-radius:0;-moz-border-radius:0;-ms-border-radius:0;border-radius:0}.fml-widget__map .ymaps-2-1-79-searchbox-input.ymaps-2-1-79-_focused{box-shadow:inset 0 0 0 2px #0073bc}.fml-widget__map .ymaps-2-1-79-searchbox-button{height:36px;background-color:#0073bc;border-color:#0073bc}.fml-widget__map .ymaps-2-1-79-searchbox-button-text{font-size:13px;line-height:36px;padding:0 12px 0 42px;color:#fff;position:relative}.fml-widget__map .ymaps-2-1-79-searchbox-button-text:before{content:"";position:absolute;top:50%;left:12px;width:20px;height:16px;background:url(\'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16" fill="none"><path d="M16.8731 9.03039L19.9526 15.3566C20.0778 15.6139 19.9462 15.8244 19.6601 15.8244H0.339955C0.0538124 15.8244 -0.0778555 15.6139 0.0474059 15.3566L3.12695 9.03039C3.15625 8.97764 3.19846 8.93318 3.24962 8.90118C3.30079 8.86918 3.35924 8.85068 3.4195 8.84742H6.06005C6.15281 8.85285 6.24011 8.89301 6.3046 8.9599C6.48375 9.16693 6.66623 9.36919 6.84819 9.56938C7.021 9.75941 7.19473 9.95193 7.36708 10.149H4.23339C4.17313 10.1522 4.11467 10.1707 4.06351 10.2027C4.01234 10.2347 3.97013 10.2792 3.94084 10.3319L1.90077 14.5228H18.0991L16.0592 10.3319C16.0299 10.2792 15.9877 10.2347 15.9366 10.2027C15.8854 10.1707 15.8269 10.1522 15.7667 10.149H12.6259C12.7982 9.95193 12.972 9.75942 13.1448 9.56938C13.3273 9.3686 13.5108 9.16674 13.6904 8.95964C13.7548 8.89291 13.8419 8.85285 13.9345 8.84742H16.5806C16.6409 8.85067 16.6993 8.86917 16.7505 8.90117C16.8016 8.93317 16.8438 8.97764 16.8731 9.03039ZM14.5275 4.70713C14.5275 8.16386 11.6498 8.81216 10.2964 12.1341C10.2721 12.1938 10.2306 12.245 10.177 12.2809C10.1235 12.3169 10.0604 12.336 9.99595 12.3359C9.93147 12.3357 9.8685 12.3163 9.81512 12.2801C9.76174 12.2439 9.7204 12.1926 9.69639 12.1328C8.47551 9.13765 6.01541 8.31631 5.54473 5.64807C5.0813 3.02131 6.89999 0.448236 9.55548 0.197223C10.1847 0.135796 10.8198 0.206706 11.42 0.405394C12.0202 0.604082 12.5721 0.926152 13.0404 1.3509C13.5087 1.77565 13.8829 2.29368 14.139 2.8717C14.3951 3.44971 14.5275 4.07491 14.5275 4.70713ZM12.3894 4.70713C12.3894 4.2339 12.249 3.7713 11.9861 3.37783C11.7232 2.98435 11.3495 2.67768 10.9123 2.49658C10.4751 2.31548 9.99403 2.2681 9.5299 2.36042C9.06577 2.45274 8.63943 2.68062 8.30481 3.01525C7.97019 3.34987 7.74231 3.7762 7.64998 4.24034C7.55766 4.70447 7.60504 5.18556 7.78614 5.62277C7.96724 6.05997 8.27391 6.43366 8.66739 6.69657C9.06086 6.95948 9.52346 7.09981 9.99669 7.09981C10.3109 7.09981 10.622 7.03792 10.9123 6.91768C11.2026 6.79744 11.4664 6.62119 11.6886 6.39901C11.9108 6.17683 12.087 5.91306 12.2072 5.62277C12.3275 5.33247 12.3894 5.02134 12.3894 4.70713H12.3894Z" fill="white"/></svg>\');background-repeat:no-repeat;background-size:cover;background-position:center;-moz-transform:translate(0,-50%);-o-transform:translate(0,-50%);-ms-transform:translate(0,-50%);-webkit-transform:translate(0,-50%);transform:translate(0,-50%)}.fml-widget__map .ymaps-2-1-79-searchbox-button:hover{background-color:#fff;color:#0073bc;border-color:#0073bc}.fml-widget__map .ymaps-2-1-79-searchbox-button:hover .ymaps-2-1-79-searchbox-button-text{color:#0073bc}.fml-widget__map .ymaps-2-1-79-searchbox-button:hover .ymaps-2-1-79-searchbox-button-text:before{background:url(\'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16" fill="none"><path d="M16.8731 9.03039L19.9526 15.3566C20.0778 15.6139 19.9462 15.8244 19.6601 15.8244H0.339955C0.0538124 15.8244 -0.0778555 15.6139 0.0474059 15.3566L3.12695 9.03039C3.15625 8.97764 3.19846 8.93318 3.24962 8.90118C3.30079 8.86918 3.35924 8.85068 3.4195 8.84742H6.06005C6.15281 8.85285 6.24011 8.89301 6.3046 8.9599C6.48375 9.16693 6.66623 9.36919 6.84819 9.56938C7.021 9.75941 7.19473 9.95193 7.36708 10.149H4.23339C4.17313 10.1522 4.11467 10.1707 4.06351 10.2027C4.01234 10.2347 3.97013 10.2792 3.94084 10.3319L1.90077 14.5228H18.0991L16.0592 10.3319C16.0299 10.2792 15.9877 10.2347 15.9366 10.2027C15.8854 10.1707 15.8269 10.1522 15.7667 10.149H12.6259C12.7982 9.95193 12.972 9.75942 13.1448 9.56938C13.3273 9.3686 13.5108 9.16674 13.6904 8.95964C13.7548 8.89291 13.8419 8.85285 13.9345 8.84742H16.5806C16.6409 8.85067 16.6993 8.86917 16.7505 8.90117C16.8016 8.93317 16.8438 8.97764 16.8731 9.03039ZM14.5275 4.70713C14.5275 8.16386 11.6498 8.81216 10.2964 12.1341C10.2721 12.1938 10.2306 12.245 10.177 12.2809C10.1235 12.3169 10.0604 12.336 9.99595 12.3359C9.93147 12.3357 9.8685 12.3163 9.81512 12.2801C9.76174 12.2439 9.7204 12.1926 9.69639 12.1328C8.47551 9.13765 6.01541 8.31631 5.54473 5.64807C5.0813 3.02131 6.89999 0.448236 9.55548 0.197223C10.1847 0.135796 10.8198 0.206706 11.42 0.405394C12.0202 0.604082 12.5721 0.926152 13.0404 1.3509C13.5087 1.77565 13.8829 2.29368 14.139 2.8717C14.3951 3.44971 14.5275 4.07491 14.5275 4.70713ZM12.3894 4.70713C12.3894 4.2339 12.249 3.7713 11.9861 3.37783C11.7232 2.98435 11.3495 2.67768 10.9123 2.49658C10.4751 2.31548 9.99403 2.2681 9.5299 2.36042C9.06577 2.45274 8.63943 2.68062 8.30481 3.01525C7.97019 3.34987 7.74231 3.7762 7.64998 4.24034C7.55766 4.70447 7.60504 5.18556 7.78614 5.62277C7.96724 6.05997 8.27391 6.43366 8.66739 6.69657C9.06086 6.95948 9.52346 7.09981 9.99669 7.09981C10.3109 7.09981 10.622 7.03792 10.9123 6.91768C11.2026 6.79744 11.4664 6.62119 11.6886 6.39901C11.9108 6.17683 12.087 5.91306 12.2072 5.62277C12.3275 5.33247 12.3894 5.02134 12.3894 4.70713H12.3894Z" fill="%230073BC"/></svg>\')}.fml-widget__scroll::-webkit-scrollbar{width:5px;-webkit-border-radius:333px;-moz-border-radius:333px;-ms-border-radius:333px;border-radius:333px}.fml-widget__scroll::-webkit-scrollbar-thumb{-webkit-border-radius:333px;-moz-border-radius:333px;-ms-border-radius:333px;border-radius:333px;background-color:#ededed}.fml-widget__button{width:70px;height:28px;color:#fff;background:#0073bc;border:1px solid #0073bc;cursor:pointer;font-size:13px;font-family:Roboto,sans-serif;font-weight:500;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;align-items:center;justify-content:center;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s}@media only screen and (min-width:992px){.fml-widget__button:hover{color:#0073bc;background:#fff}}.fml-widget__disabled .fml-widget__panel-content__status-icon:before{content:url(\'data:image/svg+xml; utf8, <svg xmlns="http://www.w3.org/2000/svg" width="22" height="18" viewBox="0 0 22 18" fill="none"><path fill="%23AAAAAA" d="M1 1.27L2.28 0L19 16.72L17.73 18L14.65 14.92C13.5 15.3 12.28 15.5 11 15.5C6 15.5 1.73 12.39 0 8C0.69 6.24 1.79 4.69 3.19 3.46L1 1.27ZM11 5C11.7956 5 12.5587 5.31607 13.1213 5.87868C13.6839 6.44129 14 7.20435 14 8C14 8.35 13.94 8.69 13.83 9L10 5.17C10.31 5.06 10.65 5 11 5ZM11 0.5C16 0.5 20.27 3.61 22 8C21.18 10.08 19.79 11.88 18 13.19L16.58 11.76C17.94 10.82 19.06 9.54 19.82 8C18.17 4.64 14.76 2.5 11 2.5C9.91 2.5 8.84 2.68 7.84 3L6.3 1.47C7.74 0.85 9.33 0.5 11 0.5ZM2.18 8C3.83 11.36 7.24 13.5 11 13.5C11.69 13.5 12.37 13.43 13 13.29L10.72 11C9.29 10.85 8.15 9.71 8 8.28L4.6 4.87C3.61 5.72 2.78 6.78 2.18 8Z"/></svg>\')!important}.fml-widget__disabled .fml-widget__panel-content__logo img{-webkit-filter:grayscale(100%);-moz-filter:grayscale(100%);-ms-filter:grayscale(100%);-o-filter:grayscale(100%);filter:grayscale(100%);filter:gray;opacity:.5}.fml-widget__disabled .fml-widget__panel-content__info{opacity:.5}.fml-widget__primary-title{text-align:center;font-size:24px;line-height:1.17;color:#333;font-family:Roboto,sans-serif;font-weight:400;margin-bottom:10px;padding:16px 16px 15px;border-bottom:1px solid rgba(0,0,0,.1)}.fml-widget__primary-title span{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:238px;padding-left:25px}@media only screen and (max-width:767px){.fml-widget__primary-title{padding:16px 0 15px;font-size:20px;height:60px;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;align-items:center;justify-content:center}}.fml-widget__sidebar-burger{position:relative}.fml-widget__sidebar-burger:before{content:"";position:absolute;left:12px;right:12px;bottom:0;height:1px;background:#ddd}.fml-widget__sidebar-button{width:100%;height:60px;cursor:pointer;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;align-items:center;justify-content:center;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s;position:relative}.fml-widget__sidebar-button:after{content:"";position:absolute;left:0;top:50%;width:0;height:0;opacity:0;visibility:hidden;border-style:solid;border-width:10px 0 10px 10px;border-color:transparent transparent transparent #fff;-moz-transform:translate(0,-50%);-o-transform:translate(0,-50%);-ms-transform:translate(0,-50%);-webkit-transform:translate(0,-50%);transform:translate(0,-50%)}.fml-widget__sidebar-button.current{background:rgba(51,51,51,.15)}.fml-widget__sidebar-button.current:after{opacity:1;visibility:visible}.fml-widget__sidebar-button-setting-icon{position:absolute;top:5px;right:5px;width:20px;height:20px;background:red;-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;border-radius:50%;display:none}.fml-widget__sidebar-button-setting-icon.current{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;align-items:center;justify-content:center}@media only screen and (min-width:992px){.fml-widget__sidebar-button:hover{background:rgba(51,51,51,.15)}}.fml-widget__sidebar-button-back-text{font-size:10px;color:#0073bc;font-family:Roboto,sans-serif;font-weight:500;text-align:center;margin-bottom:4px}.fml-widget__sidebar-button-wrap{flex-grow:1;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.fml-widget__sidebar-button-checked.current{background:rgba(51,51,51,.15)}.fml-widget__sidebar-button-checked.current:after{display:none}.fml-widget__sidebar-button__hint{height:30px;position:absolute;right:-300px;background:rgba(0,115,188,.95);color:#fff;padding:5px 12px;border-radius:30px;cursor:default;opacity:0;z-index:6;top:50%;width:max-content;-webkit-border-radius:30px;-moz-border-radius:30px;-ms-border-radius:30px;border-radius:30px;-webkit-transition:right .5s ease-out,opacity .3s ease-out;-moz-transition:right .5s ease-out,opacity .3s ease-out;-ms-transition:right .5s ease-out,opacity .3s ease-out;-o-transition:right .5s ease-out,opacity .3s ease-out;transition:right .5s ease-out,opacity .3s ease-out}.fml-widget__sidebar-button__hint.show{right:67px;opacity:1}.fml-widget__sidebar-button__hint.fml-widget-list{top:17px}@media only screen and (max-width:767px){.fml-widget__sidebar-button__hint.fml-widget-list{top:76px}}.fml-widget__sidebar-button__hint.fml-widget-cash{top:76px}@media only screen and (max-width:767px){.fml-widget__sidebar-button__hint.fml-widget-cash{top:134px}}.fml-widget__sidebar-button__hint.fml-widget-cal{top:134px}@media only screen and (max-width:767px){.fml-widget__sidebar-button__hint.fml-widget-cal{top:195px}}.fml-widget__sidebar-button__hint.fml-widget-delivery{top:195px}@media only screen and (max-width:767px){.fml-widget__sidebar-button__hint.fml-widget-delivery{top:254px}}.fml-widget__price{font-family:Roboto,sans-serif;font-weight:700;color:#0073bc}.fml-widget__days{font-family:Roboto,sans-serif;font-weight:400}.fml-widget__delivery-container{padding:0 16px;border-bottom:1px solid rgba(0,0,0,.1)}@media only screen and (max-width:767px){.fml-widget__delivery-container{padding:0 10px}}.fml-widget__delivery-type{position:absolute;top:0;bottom:0;right:-460px;width:460px;z-index:7;background:rgba(255,255,255,.95);-webkit-transition:right .2s ease-out,opacity .1s ease-out;-moz-transition:right .2s ease-out,opacity .1s ease-out;-ms-transition:right .2s ease-out,opacity .1s ease-out;-o-transition:right .2s ease-out,opacity .1s ease-out;transition:right .2s ease-out,opacity .1s ease-out}.fml-widget__delivery-type.current{right:0;-webkit-transition:right .5s ease-out,opacity .3s ease-out;-moz-transition:right .5s ease-out,opacity .3s ease-out;-ms-transition:right .5s ease-out,opacity .3s ease-out;-o-transition:right .5s ease-out,opacity .3s ease-out;transition:right .5s ease-out,opacity .3s ease-out}@media only screen and (max-width:767px){.fml-widget__delivery-type{top:56px;bottom:40px;right:-calc(100% - 20px);z-index:2}.fml-widget__delivery-type.current{width:calc(100% - 20px);right:10px;-webkit-transition:right .5s ease-out,opacity .3s ease-out;-moz-transition:right .5s ease-out,opacity .3s ease-out;-ms-transition:right .5s ease-out,opacity .3s ease-out;-o-transition:right .5s ease-out,opacity .3s ease-out;transition:right .5s ease-out,opacity .3s ease-out}}.fml-widget__delivery-type__options-content{height:calc(100% - 126px);overflow:auto}@media only screen and (max-width:767px){.fml-widget__delivery-type__options-content{height:calc(100% - 160px)}}.fml-widget__delivery-type__options{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex}.fml-widget__delivery-type__item{padding:8px 20px;width:50%;cursor:pointer;opacity:.33;font-size:14px;color:#000;font-family:Roboto,sans-serif;font-weight:500;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s}.fml-widget__delivery-type__item.current,.fml-widget__delivery-type__item:hover{opacity:1;background:rgba(51,51,51,.15)}.fml-widget__delivery-type__item-details span{display:block}@media only screen and (max-width:767px){.fml-widget__delivery-type__item{width:auto;padding:8px 12px}}.fml-widget__delivery-type__list-item{padding:16px 10px;height:88px;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;cursor:pointer}.fml-widget__delivery-type__list-item:hover{background:rgba(51,51,51,.15)}.fml-widget__delivery-type__info{flex-grow:1}.fml-widget__delivery-type__info-date,.fml-widget__delivery-type__info-type{display:block;font-size:15px;line-height:1.17;color:#333;font-family:Roboto,sans-serif;font-weight:400}.fml-widget__delivery-type__info-date span,.fml-widget__delivery-type__info-type span{font-family:Roboto,sans-serif;font-weight:500}.fml-widget__delivery-type__logo{width:60px;max-height:88px;flex-shrink:0;margin-right:16px}.fml-widget__delivery-type__logo img{width:auto;height:auto;max-width:100%;max-height:100%}.fml-widget__delivery-type__price-wrap{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.fml-widget__delivery-type__price{font-family:Roboto,sans-serif;font-weight:500;font-size:20px;line-height:1.15;color:#0073bc}.fml-widget__delivery-type__button{width:70px;height:28px;color:#fff;background:#0073bc;border:1px solid #0073bc;cursor:pointer;font-size:13px;font-family:Roboto,sans-serif;font-weight:500;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;align-items:center;justify-content:center;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s}@media only screen and (min-width:992px){.fml-widget__delivery-type__button:hover{color:#0073bc;background:#fff}}.fml-widget__delivery-button{color:#0073bc;background:#fff;height:36px;padding:0 12px;cursor:pointer;border:none;position:relative;z-index:2;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s;-webkit-box-shadow:0 2px 2px rgba(0,0,0,.15);-moz-box-shadow:0 2px 2px rgba(0,0,0,.15);box-shadow:0 2px 2px rgba(0,0,0,.15)}.fml-widget__delivery-button svg{margin-right:10px;fill:#0073bc}.fml-widget__delivery-button.current,.fml-widget__delivery-button:hover{color:#fff;background:#0073bc;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}.fml-widget__delivery-button.current svg,.fml-widget__delivery-button:hover svg{fill:#fff}.fml-widget__delivery-button-wrap{position:absolute;top:10px;right:70px;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex}@media only screen and (max-width:767px){.fml-widget__delivery-button-wrap{display:none!important}}.fml-widget{-webkit-perspective:2000px;perspective:2000px}.fml-widget__panel{width:400px;height:100%;overflow:hidden;position:absolute;top:0;right:60px;z-index:6;opacity:0;background:rgba(255,255,255,.95);-webkit-transform-origin:right;-ms-transform-origin:right;transform-origin:right;-moz-transform:rotateY(-101deg);-o-transform:rotateY(-101deg);-ms-transform:rotateY(-101deg);-webkit-transform:rotateY(-101deg);transform:rotateY(-101deg);-webkit-transition:transform .3s linear,opacity .5s ease-in;-moz-transition:transform .3s linear,opacity .5s ease-in;-ms-transition:transform .3s linear,opacity .5s ease-in;-o-transition:transform .3s linear,opacity .5s ease-in;transition:transform .3s linear,opacity .5s ease-in}@media only screen and (max-width:767px){.fml-widget__panel{top:59px;width:calc(100% - 70px);height:auto;bottom:0}}.fml-widget__panel.open{opacity:1;-moz-transform:rotateY(0);-o-transform:rotateY(0);-ms-transform:rotateY(0);-webkit-transform:rotateY(0);transform:rotateY(0)}.fml-widget__panel>div{height:100%;width:400px;position:absolute;top:0;right:-400px}@media only screen and (max-width:767px){.fml-widget__panel>div{width:100%;height:auto;bottom:0;right:-100%}}.fml-widget__panel-list{left:0;-webkit-transition:left ease .5s;-moz-transition:left ease .5s;-ms-transition:left ease .5s;-o-transition:left ease .5s;transition:left ease .5s}.fml-widget__panel-list.current{left:-400px}@media only screen and (max-width:767px){.fml-widget__panel-list.current{left:-100%}}.fml-widget__panel-list-delivery .fml-widget__panel-content_list-item{height:78px;padding:16px 18px;cursor:pointer}.fml-widget__panel-list-delivery .fml-widget__panel-content__status{width:22px;height:15px;margin-right:8px;position:relative;flex-shrink:0}.fml-widget__panel-list-delivery .fml-widget__panel-content__status-icon{position:absolute;top:0;left:0;right:0;bottom:0;width:22px;height:15px}.fml-widget__panel-list-delivery .fml-widget__panel-content__status-icon:before{content:url(\'data:image/svg+xml; utf8, <svg xmlns="http://www.w3.org/2000/svg" width="22" height="16" viewBox="0 0 22 16" fill="none"><path d="M11 5C10.2044 5 9.44129 5.31607 8.87868 5.87868C8.31607 6.44129 8 7.20435 8 8C8 8.79565 8.31607 9.55871 8.87868 10.1213C9.44129 10.6839 10.2044 11 11 11C11.7956 11 12.5587 10.6839 13.1213 10.1213C13.6839 9.55871 14 8.79565 14 8C14 7.20435 13.6839 6.44129 13.1213 5.87868C12.5587 5.31607 11.7956 5 11 5ZM11 13C9.67392 13 8.40215 12.4732 7.46447 11.5355C6.52678 10.5979 6 9.32608 6 8C6 6.67392 6.52678 5.40215 7.46447 4.46447C8.40215 3.52678 9.67392 3 11 3C12.3261 3 13.5979 3.52678 14.5355 4.46447C15.4732 5.40215 16 6.67392 16 8C16 9.32608 15.4732 10.5979 14.5355 11.5355C13.5979 12.4732 12.3261 13 11 13ZM11 0.5C6 0.5 1.73 3.61 0 8C1.73 12.39 6 15.5 11 15.5C16 15.5 20.27 12.39 22 8C20.27 3.61 16 0.5 11 0.5Z" fill="%230073BC"/></svg>\');width:100%}.fml-widget__panel-list-points{height:100%}.fml-widget__panel-content{height:calc(100% - 75px);overflow:auto}@media only screen and (max-width:767px){.fml-widget__panel-content-wrap{height:100%}}.fml-widget__panel-content_list-item{height:74px;padding:16px 10px;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;cursor:pointer;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s}.fml-widget__panel-content_list-item:hover{background:rgba(51,51,51,.15)}.fml-widget__panel-content__info{flex-grow:1}.fml-widget__panel-content__info-address,.fml-widget__panel-content__info-amount,.fml-widget__panel-content__info-delivery{display:block;font-size:16px;line-height:1.17;color:#333;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:238px}.fml-widget__panel-content__info-address,.fml-widget__panel-content__info-amount{font-family:Roboto,sans-serif;font-weight:500}.fml-widget__panel-content__info-delivery{font-family:Roboto,sans-serif;font-weight:400}.fml-widget__panel-content__info-delivery span{font-family:Roboto,sans-serif;font-weight:500;color:#0073bc}.fml-widget__panel-content__logo{width:60px;max-height:74px;flex-shrink:0;margin-right:16px;text-align:center}.fml-widget__panel-content__logo img{width:auto;height:auto;max-width:100%;max-height:100%;vertical-align:middle}.fml-widget__select{position:relative;display:block;min-width:158px;width:100%;max-width:158px;margin-right:16px}@media only screen and (max-width:767px){.fml-widget__select{min-width:100%;margin:0 0 8px}}.fml-widget__select__head{width:100%;max-width:100%;padding:9px 12px;font-size:16px;line-height:1.19;color:#333;cursor:pointer;border:2px solid #ddd;-webkit-border-radius:4px;-moz-border-radius:4px;-ms-border-radius:4px;border-radius:4px}.fml-widget__select__head svg{width:7px;position:absolute;right:14px;bottom:50%;-webkit-transition:.2s ease-in;-moz-transition:.2s ease-in;-ms-transition:.2s ease-in;-o-transition:.2s ease-in;transition:.2s ease-in;-moz-transform:translateY(50%) rotate(-90deg);-o-transform:translateY(50%) rotate(-90deg);-ms-transform:translateY(50%) rotate(-90deg);-webkit-transform:translateY(50%) rotate(-90deg);transform:translateY(50%) rotate(-90deg)}.fml-widget__select__head svg .path{fill:#0073bc}.fml-widget__select__head.open{-webkit-border-radius:4px 4px 0 0;-moz-border-radius:4px 4px 0 0;-ms-border-radius:4px 4px 0 0;border-radius:4px 4px 0 0}.fml-widget__select__head.open svg{transform:translateY(50%) rotate(90deg)}.fml-widget__select__list{display:none;position:absolute;top:100%;left:0;right:0;background:#fff;overflow-x:hidden;overflow-y:auto;z-index:100;margin:-2px 0 0;padding:0;font-size:16px;line-height:1.19;color:#333;border:2px solid #ddd;border-top:none;-webkit-border-radius:0 0 4px 4px;-moz-border-radius:0 0 4px 4px;-ms-border-radius:0 0 4px 4px;border-radius:0 0 4px 4px}.fml-widget__select__item{position:relative;border-top:1px solid rgba(224,229,231,.5);padding:10px 15px;cursor:pointer;list-style-type:none}.fml-widget__select__item:hover{background-color:rgba(224,229,231,.5)}.fml-widget__panel-details{-webkit-transition:right ease .5s;transition:right ease .5s}.fml-widget__panel-details.current{right:0}.fml-widget__panel-details__list{height:calc(100% - 75px);overflow:auto}.fml-widget__panel-details__back{width:46px;height:60px;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;position:absolute;top:0;left:0;cursor:pointer;z-index:4}.fml-widget__panel-details__back svg{width:12px;height:25px;margin:auto}.fml-widget__panel-details__back svg .path{fill:#0073bc;-webkit-transition:.2s;-moz-transition:.2s;-ms-transition:.2s;-o-transition:.2s;transition:.2s}.fml-widget__panel-details__back:hover svg .path{fill:#333}.fml-widget__panel-details__item{padding:0 16px}.fml-widget__panel-details__item-header{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.fml-widget__panel-details__logo{width:60px;height:24px;flex-shrink:0;margin-right:16px}.fml-widget__panel-details__logo img{width:auto;height:auto;max-width:100%;max-height:100%}.fml-widget__panel-details__info{flex-grow:1;margin-bottom:8px}.fml-widget__panel-details__info span{display:block}.fml-widget__panel-details__info-amount,.fml-widget__panel-details__info-delivery{display:block;font-size:16px;line-height:1.17;color:#333}.fml-widget__panel-details__info-amount{font-family:Roboto,sans-serif;font-weight:500}.fml-widget__panel-details__info-delivery{font-family:Roboto,sans-serif;font-weight:400}.fml-widget__panel-details__info-delivery span{font-family:Roboto,sans-serif;font-weight:500;color:#0073bc}.fml-widget__panel-details__price-wrap{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.fml-widget__panel-details__address-wrap,.fml-widget__panel-details__description-wrap,.fml-widget__panel-details__phones-wrap,.fml-widget__panel-details__price-wrap,.fml-widget__panel-details__working-hours-wrap{margin-bottom:16px}.fml-widget__panel-details__price{font-family:Roboto,sans-serif;font-weight:500;color:#0073bc;font-size:20px;line-height:1.15;margin-right:16px}.fml-widget__panel-details__title{width:100%;color:#777;font-size:14px;line-height:1.17;margin-bottom:4px;font-family:Roboto,sans-serif;font-weight:400}.fml-widget__panel-details__info-wrap{color:#333;font-size:16px;line-height:1.17;font-family:Roboto,sans-serif;font-weight:400}.fml-widget__panel-details__info-wrap span{display:block}.fml-widget__panel-details__phones-wrap .fml-widget__panel-details__info-wrap{display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.fml-widget__panel-details__phones-wrap .fml-widget__panel-details__info-phone{color:#333;text-decoration:none;-webkit-transition:all .2s;-moz-transition:all .2s;-ms-transition:all .2s;-o-transition:all .2s;transition:all .2s}.fml-widget__panel-details__phones-wrap .fml-widget__panel-details__info-phone:hover{color:#000}.fml-widget__sidebar{width:60px;height:100%;position:absolute;top:0;right:-60px;padding:0;z-index:7;display:-webkit-box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;background:rgba(255,255,255,.95);-moz-transform:translateX(0);-o-transform:translateX(0);-ms-transform:translateX(0);-webkit-transform:translateX(0);transform:translateX(0);-webkit-transition:right .5s ease-out,opacity .3s ease-out;-moz-transition:right .5s ease-out,opacity .3s ease-out;-ms-transition:right .5s ease-out,opacity .3s ease-out;-o-transition:right .5s ease-out,opacity .3s ease-out;transition:right .5s ease-out,opacity .3s ease-out}.fml-widget__sidebar.current{right:0}@media only screen and (max-width:767px){.fml-widget__sidebar{top:59px;height:auto;bottom:0;padding-bottom:59px}}*{-webkit-box-sizing:border-box;box-sizing:border-box}:after,:before{-webkit-box-sizing:border-box;box-sizing:border-box}button,img,input,textarea{outline:0;-webkit-appearance:none;-moz-appearance:none}.fml-widget{max-width:962px;width:100%;position:relative;border:1px solid rgba(0,0,0,.1);overflow:hidden}@media only screen and (max-width:767px){.fml-widget__map{z-index:2;position:relative}}.fml-widget__first-hint{position:absolute;top:58px;left:10px;background:#fff;border:1px solid #f1f1ec;padding:8px;z-index:2;font-size:13px;line-height:1.15;font-family:Roboto,sans-serif;font-weight:400;-webkit-box-shadow:0 2px 2px rgba(0,0,0,.15);-moz-box-shadow:0 2px 2px rgba(0,0,0,.15);box-shadow:0 2px 2px rgba(0,0,0,.15)}@media only screen and (max-width:767px){.fml-widget__first-hint{right:10px}}',
		PlaceMarksStyles: '.fml-widget-selected-placemark {position: absolute;top: -40px;left: -20px;width: 40px!important;height: 43px!important;max-width: 40px!important;max-height: 43px!important;content: url(\'data:image/svg+xml; utf-8, <svg width="40" height="43" viewBox="0 0 40 43" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(%23clip0)"><g filter="url(%23filter0_f)"><path d="M20 39.9999L7.27201 29.1628C4.75468 27.0195 3.04037 24.2886 2.34586 21.3157C1.65134 18.3428 2.00781 15.2613 3.3702 12.4608C4.73258 9.6604 7.03968 7.26683 9.99976 5.58281C12.9598 3.89878 16.4399 2.99994 20 2.99994C23.5601 2.99994 27.0402 3.89878 30.0002 5.58281C32.9603 7.26683 35.2674 9.6604 36.6298 12.4608C37.9922 15.2613 38.3487 18.3428 37.6541 21.3157C36.9596 24.2886 35.2453 27.0195 32.728 29.1628L20 39.9999Z" fill="url(%23paint0_linear)"/></g><g filter="url(%23filter1_f)"><path d="M20 39.9999L8.68623 29.1628C6.44861 27.0195 4.92478 24.2886 4.30743 21.3157C3.69008 18.3428 4.00695 15.2613 5.21795 12.4608C6.42896 9.6604 8.47972 7.26683 11.1109 5.58281C13.7421 3.89878 16.8355 2.99994 20 2.99994C23.1645 2.99994 26.2579 3.89878 28.8891 5.58281C31.5203 7.26683 33.571 9.6604 34.782 12.4608C35.9931 15.2613 36.3099 18.3428 35.6926 21.3157C35.0752 24.2886 33.5514 27.0195 31.3138 29.1628L20 39.9999Z" fill="url(%23paint1_linear)"/></g><path d="M19.3514 38.6305L20 39.2892L20.6486 38.6305L30.5482 28.5776L29.8996 27.9389L30.5482 28.5776C32.6323 26.4612 34.0501 23.7663 34.6244 20.8346C35.1986 17.9029 34.904 14.8641 33.7772 12.1017C32.6504 9.33916 30.7412 6.97574 28.2891 5.31189C25.8368 3.64792 22.9521 2.75889 20 2.75889C17.0479 2.75889 14.1632 3.64792 11.7109 5.31189C9.25875 6.97574 7.34963 9.33916 6.2228 12.1017C5.09602 14.8641 4.80141 17.9029 5.37564 20.8346C5.94989 23.7663 7.36766 26.4612 9.45181 28.5776L10.1004 27.9389L9.45182 28.5776L19.3514 38.6305Z" fill="%23599CB9" stroke="white" stroke-width="1.82067"/><circle cx="20" cy="18" r="12" fill="white"/></g><defs><filter id="filter0_f" x="-1.64134" y="-0.641397" width="43.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><filter id="filter1_f" x="0.358664" y="-0.641397" width="39.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><linearGradient id="paint0_linear" x1="25.1257" y1="2.99994" x2="25.1257" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><linearGradient id="paint1_linear" x1="24.5562" y1="2.99994" x2="24.5562" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><clipPath id="clip0"><rect width="40" height="43" fill="white"/></clipPath></defs></svg>\');}  .fml-widget-selected-placemark.selected{content: url(\'data:image/svg+xml; utf-8, <svg width="40" height="43" viewBox="0 0 40 43" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(%23clip0)"><g filter="url(%23filter0_f)"><path d="M20 39.9999L7.27201 29.1628C4.75468 27.0195 3.04037 24.2886 2.34586 21.3157C1.65134 18.3428 2.00781 15.2613 3.3702 12.4608C4.73258 9.6604 7.03968 7.26683 9.99976 5.58281C12.9598 3.89878 16.4399 2.99994 20 2.99994C23.5601 2.99994 27.0402 3.89878 30.0002 5.58281C32.9603 7.26683 35.2674 9.6604 36.6298 12.4608C37.9922 15.2613 38.3487 18.3428 37.6541 21.3157C36.9596 24.2886 35.2453 27.0195 32.728 29.1628L20 39.9999Z" fill="url(%23paint0_linear)"/></g><g filter="url(%23filter1_f)"><path d="M20 39.9999L8.68623 29.1628C6.44861 27.0195 4.92478 24.2886 4.30743 21.3157C3.69008 18.3428 4.00695 15.2613 5.21795 12.4608C6.42896 9.6604 8.47972 7.26683 11.1109 5.58281C13.7421 3.89878 16.8355 2.99994 20 2.99994C23.1645 2.99994 26.2579 3.89878 28.8891 5.58281C31.5203 7.26683 33.571 9.6604 34.782 12.4608C35.9931 15.2613 36.3099 18.3428 35.6926 21.3157C35.0752 24.2886 33.5514 27.0195 31.3138 29.1628L20 39.9999Z" fill="url(%23paint1_linear)"/></g><path d="M19.3514 38.6305L20 39.2892L20.6486 38.6305L30.5482 28.5776L29.8996 27.9389L30.5482 28.5776C32.6323 26.4612 34.0501 23.7663 34.6244 20.8346C35.1986 17.9029 34.904 14.8641 33.7772 12.1017C32.6504 9.33916 30.7412 6.97574 28.2891 5.31189C25.8368 3.64792 22.9521 2.75889 20 2.75889C17.0479 2.75889 14.1632 3.64792 11.7109 5.31189C9.25875 6.97574 7.34963 9.33916 6.2228 12.1017C5.09602 14.8641 4.80141 17.9029 5.37564 20.8346C5.94989 23.7663 7.36766 26.4612 9.45181 28.5776L10.1004 27.9389L9.45182 28.5776L19.3514 38.6305Z" fill="%2300000082" stroke="white" stroke-width="1.82067"/><circle cx="20" cy="18" r="12" fill="white"/></g><defs><filter id="filter0_f" x="-1.64134" y="-0.641397" width="43.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><filter id="filter1_f" x="0.358664" y="-0.641397" width="39.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><linearGradient id="paint0_linear" x1="25.1257" y1="2.99994" x2="25.1257" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><linearGradient id="paint1_linear" x1="24.5562" y1="2.99994" x2="24.5562" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><clipPath id="clip0"><rect width="40" height="43" fill="white"/></clipPath></defs></svg>\');}  .fml-widget-selected-placemark:hover {content: url(\'data:image/svg+xml; utf-8, <svg width="40" height="43" viewBox="0 0 40 43" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(%23clip0)"><g filter="url(%23filter0_f)"><path d="M20 39.9999L7.27201 29.1628C4.75468 27.0195 3.04037 24.2886 2.34586 21.3157C1.65134 18.3428 2.00781 15.2613 3.3702 12.4608C4.73258 9.6604 7.03968 7.26683 9.99976 5.58281C12.9598 3.89878 16.4399 2.99994 20 2.99994C23.5601 2.99994 27.0402 3.89878 30.0002 5.58281C32.9603 7.26683 35.2674 9.6604 36.6298 12.4608C37.9922 15.2613 38.3487 18.3428 37.6541 21.3157C36.9596 24.2886 35.2453 27.0195 32.728 29.1628L20 39.9999Z" fill="url(%23paint0_linear)"/></g><g filter="url(%23filter1_f)"><path d="M20 39.9999L8.68623 29.1628C6.44861 27.0195 4.92478 24.2886 4.30743 21.3157C3.69008 18.3428 4.00695 15.2613 5.21795 12.4608C6.42896 9.6604 8.47972 7.26683 11.1109 5.58281C13.7421 3.89878 16.8355 2.99994 20 2.99994C23.1645 2.99994 26.2579 3.89878 28.8891 5.58281C31.5203 7.26683 33.571 9.6604 34.782 12.4608C35.9931 15.2613 36.3099 18.3428 35.6926 21.3157C35.0752 24.2886 33.5514 27.0195 31.3138 29.1628L20 39.9999Z" fill="url(%23paint1_linear)"/></g><path d="M19.3514 38.6305L20 39.2892L20.6486 38.6305L30.5482 28.5776L29.8996 27.9389L30.5482 28.5776C32.6323 26.4612 34.0501 23.7663 34.6244 20.8346C35.1986 17.9029 34.904 14.8641 33.7772 12.1017C32.6504 9.33916 30.7412 6.97574 28.2891 5.31189C25.8368 3.64792 22.9521 2.75889 20 2.75889C17.0479 2.75889 14.1632 3.64792 11.7109 5.31189C9.25875 6.97574 7.34963 9.33916 6.2228 12.1017C5.09602 14.8641 4.80141 17.9029 5.37564 20.8346C5.94989 23.7663 7.36766 26.4612 9.45181 28.5776L10.1004 27.9389L9.45182 28.5776L19.3514 38.6305Z" fill="%2300000082" stroke="white" stroke-width="1.82067"/><circle cx="20" cy="18" r="12" fill="white"/></g><defs><filter id="filter0_f" x="-1.64134" y="-0.641397" width="43.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><filter id="filter1_f" x="0.358664" y="-0.641397" width="39.2827" height="44.2827" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feGaussianBlur stdDeviation="1.82067" result="effect1_foregroundBlur"/></filter><linearGradient id="paint0_linear" x1="25.1257" y1="2.99994" x2="25.1257" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><linearGradient id="paint1_linear" x1="24.5562" y1="2.99994" x2="24.5562" y2="39.9999" gradientUnits="userSpaceOnUse"><stop offset="0.354167" stop-opacity="0"/><stop offset="1"/></linearGradient><clipPath id="clip0"><rect width="40" height="43" fill="white"/></clipPath></defs></svg>\');}',
		MobileStyles: '.fml-widget__panel-content-wrap.fml-widget__panel-list-points > .fml-widget__panel-details__back {display: none;} @media only screen and (max-width: 989px) and (min-width: 764px) { .fml-widget div.fml-widget__panel .fml-widget__primary-title {position: relative;left: -10px;padding-left: 0;font-size: 17px;}} @media only screen and (max-width:887px) {  .fml-widget div.fml-widget__panel {width: calc(100vw/2 - 95px);}  .fml-widget div.fml-widget__delivery-type.current {width: calc(100vw/2 - 33px);}  }  @media only screen and (max-width:767px) {  .fml-widget div.fml-widget__sidebar {padding-bottom: 0;height: min-content;box-shadow: 0px 0px 4px #979797;}  .fml-widget .fml-widget__sidebar-button-back-wrap div.fml-widget__sidebar-button {height: 45px}  .fml-widget__panel-content-wrap.fml-widget__panel-list-points > .fml-widget__panel-details__back {position: absolute;right: 0;left: auto;transform: rotateY(180deg);}  .fml-widget div.fml-widget__panel {right: 7px;top: 48px;width: calc(100% - 15px);}  .fml-widget div.fml-widget__delivery-type.current {width: calc(100% - 20px);}  .fml-widget__map ymaps.ymaps-2-1-79-search.ymaps-2-1-79-search_layout_normal.ymaps-2-1-79-searchbox__normal-layout {width: 96vw;}  }',
		PopupStyles: '.fml_popup_mode .fml_popup_mode_close {content: url(\'data:image/svg+xml; utf8, <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="48" height="48" rx="4" fill="%23fff"/><path d="M33 16.8129L31.1871 15L24 22.1871L16.8129 15L15 16.8129L22.1871 24L15 31.1871L16.8129 33L24 25.8129L31.1871 33L33 31.1871L25.8129 24L33 16.8129Z" fill="%230073BC"/></svg>\');width: 45px;height: 45px;position: absolute;top: 4px;right: 8px;cursor: pointer;z-index: 100;box-shadow: 0 0 4px #979797;}.fml-widget .fml_popup_mode_close:hover {content: url(\'data:image/svg+xml; utf8, <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="48" height="48" rx="4" fill="%2333333326"/><path d="M33 16.8129L31.1871 15L24 22.1871L16.8129 15L15 16.8129L22.1871 24L15 31.1871L16.8129 33L24 25.8129L31.1871 33L33 31.1871L25.8129 24L33 16.8129Z" fill="%230073BC"/></svg>\');}  .fml_popup_mode .fml-widget__sidebar-button__hint.fml-widget-list {top: 75px!important;}  .fml_popup_mode .fml-widget__sidebar-button__hint.fml-widget-cash {top: 134px!important;}  .fml_popup_mode .fml-widget__sidebar-button__hint.fml-widget-cal {top: 191px!important;}  .fml_popup_mode.fml-widget div.fml-widget__sidebar {padding-bottom: 0!important;height: min-content!important;box-shadow: 0 0 4px #979797!important;top: 59px!important;}  @media only screen and (max-width: 989px) {.fml_popup_mode .fml-widget__primary-title span {padding-left: 0!important;font-size: 22px!important;}  .fml_popup_mode .fml-widget__primary-title {padding-left: 0!important;text-align: left!important;}} @media only screen and (max-width: 764px) {  .fml_popup_mode .fml-widget__primary-title span {max-width: none!important;text-align: center!important;font-size: 24px!important;}  .fml_popup_mode .fml-widget__map ymaps.ymaps-2-1-79-search.ymaps-2-1-79-search_layout_normal.ymaps-2-1-79-searchbox__normal-layout {width: calc(100vw - 85px)!important;}}',
	};

	const StaticAssets = {
		courier_variant_title: 'Курьер до двери',
		pvz_variant_title: 'Пункт выдачи заказов',
		change_delivery_type_title: 'Изменить способ доставки',
		work_day_variant: [' рабочий день', ' рабочих дня', 'рабочих дней'],
		variant_day: ['день', 'дня', 'дней'],
		filter_day: ['-го рабочего дня', '-х рабочих дней', '-и рабочих дней'],
		price: ' ₽',
		price_from: 'от ',
		select_type_title: 'Выберите способ доставки',
		select_text: 'Выбрать',
		back_description: 'Изменить способ доставки',
		pvz_count_template: 'Пункты выдачи (#COUNT# шт.)',
		filter_items_title: 'Курьерские компании',
		detail_price_title: 'Стоимость доставки:',
		detail_select_title: 'Выбрать',
		detail_address_title: 'Адрес пункта выдачи заказов:',
		detail_work_time_title: 'Время работы:',
		detail_phone_title: 'Телефоны:',
		detail_description_title: 'Как к нам проехать:',
		filter_item_count: ' (#COUNT# шт.)',
		panel_info_pvz: 'ПВЗ и постаматы',
		panel_info_type_1: 'Постамат',
		panel_info_type_2: 'ПВЗ',
		panel_info_pay_all: 'Любая оплата',
		panel_info_pay_cash: 'Оплата наличными',
		panel_info_pay_card: 'Оплата картой',
		panel_info_placeholder_all: 'ПВЗ и постаматы',
		panel_info_placeholder_pvz: 'только ПВЗ',
		panel_info_placeholder_terminal: 'только постаматы',
		panel_info_filter: 'Выбор курьерских компаний',
	};
	class PanelInfoAssets {
		static getPvzTitle = () => StaticAssets.panel_info_pvz;
		static getFilterPayAll = () => StaticAssets.panel_info_pay_all;
		static getFilterPayCard =() => StaticAssets.panel_info_pay_card;
		static getFilterPayCache = () => StaticAssets.panel_info_pay_cash;
		static getFilterTypeAll = () => StaticAssets.panel_info_placeholder_all;
		static getFilterTypePvz = () => StaticAssets.panel_info_placeholder_pvz;
		static getFilterTypeTerminal = () => StaticAssets.panel_info_placeholder_terminal;
		static getFilterTitle = () => StaticAssets.panel_info_filter;
	}
	class DetailAssets {
		static getPriceTitle = () => StaticAssets.detail_price_title;
		static getSelectTitle = () => StaticAssets.detail_select_title;
		static getAddressTitle = () => StaticAssets.detail_address_title;
		static getWorkTimeTitle = () => StaticAssets.detail_work_time_title;
		static getPhoneTitle = () => StaticAssets.detail_phone_title;
		static getDescTitle = () => StaticAssets.detail_description_title;
	}
	class Utility {
		static insertStyle = (Style) => {
			let styleSheet = document.createElement("style");
			styleSheet.innerHTML = Style;
			styleSheet.classList.add('fml_stylesheet_rules');
			document.body.appendChild(styleSheet);
		}
		static insertAllStyles = () => {
			Utility.insertStyle(WidgetStyleAssets.BaseWidgetStyles);
			Utility.insertStyle(WidgetStyleAssets.PlaceMarksStyles);
			Utility.insertStyle(WidgetStyleAssets.MobileStyles);
			Utility.insertStyle(WidgetStyleAssets.PopupStyles);
		}
		static removeStyles = () => {
			let styles = document.querySelectorAll('.fml_stylesheet_rules');
			for (let i in styles) {
				if (styles[i] && styles[i].tagName === 'STYLE') {
					styles[i].remove();
				}
			}
		}
		static wordByNumber = (n, text_forms) => {
			n = Math.abs(n) % 100;
			let n1 = n % 10;
			if (n > 10 && n < 20) { return text_forms[2]; }
			if (n1 > 1 && n1 < 5) { return text_forms[1]; }
			if (n1 === 1) { return text_forms[0]; }
			return text_forms[2];
		};
		static getCourierVariantTitle = () => StaticAssets.courier_variant_title;
		static getPvzVariantTitle = () => StaticAssets.pvz_variant_title;
		static getWorkDay = (number) => {
			let result = '';
			if (number > 0 || number === 0) {
				result = number + Utility.wordByNumber(number, StaticAssets.work_day_variant);
			}
			return result;
		}
		static getVariantDay = (number) => {
			let result = '';
			if (number > 0 || number === 0) {
				result = number + Utility.wordByNumber(number, StaticAssets.variant_day);
			}
			return result;
		}
		static getPrice = (price) => {
			let result = '';
			if ((price > 0 || price === 0 ) && price !== Infinity) {
				let priceStr = price + '';
				priceStr = priceStr.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				result = priceStr + StaticAssets.price;
			}
			return result;
		}
		static getPriceFrom = (price) => {
			let result = '';
			if ((price > 0 || price === 0 ) && price !== Infinity) {
				result = StaticAssets.price_from + Utility.getPrice(price);
			}
			return result;
		}
		static getFilterDateFrom = (date) => {
			let result = '';
			if (date > 0) {
				result = ', от ' + date + this.wordByNumber(date, StaticAssets.filter_day);
			}
			return result;
		}
		static getSelectTypeTitle = () => StaticAssets.select_type_title;
		static getSelectText = () => StaticAssets.select_text;
		static getButtonBuckDesc = () => StaticAssets.back_description;
		/**
		 * @return {{ButtonBack: string, Filter: string, Payment_all: string, Payment_cash: string, DetailBuck: string, Point: string, Payment_card: string, FilterActive: string}}
		 */
		static getSvgEnum = () => SvgAssets;
		static getPvzListTitle = (count) => StaticAssets.pvz_count_template.replace('#COUNT#', count);
		static getFilterListTitle = () => StaticAssets.filter_items_title;
		static getDetailAssets = () => DetailAssets;
		static getPvzItemInfo = (ItemParams) => {
			let dateText = '';
			let datePref = '';
			let priceText = '';

			if (ItemParams.price > 0 || ItemParams.price === 0) {
				priceText = '<span>' + this.getPrice(ItemParams.price);
				datePref = ', ';
			}
			if (ItemParams.date > 0 || ItemParams.date === 0) {
				dateText = datePref + this.getWorkDay(ItemParams.date)
			}
			return '<span>' + priceText + '</span>' + dateText + '</span>';
		}
		static getFilterItemCount = (count = 0) => StaticAssets.filter_item_count.replace('#COUNT#', count);
		static getPanelAssets = () => PanelInfoAssets;
	}
	class ItemCollection {
		set = (id, Item) => {
			if (this.has(id)) {
				this.items[id] = Item;
			} else {
				this.add(Item, id)
			}
		};
		replace = id => {
			delete this.items[id];
			this.ids.filter(item => item !== id);
		};
		items = {};
		ids = [];
		add = (Item, id = false) => {
			let key = id ? id : Object.keys(this.items).length;
			Item.collectionReference = this;
			this.items[key] = Item;
			this.ids.push(key);
		};
		get = id => {
			let Item = false;
			for (let key in this.items) {
				if (key === id) {
					Item = this.items[key];
					break;
				}
			}
			return Item;
		};
		has = id => this.ids.includes(id);
		each = callback => {
			this.ids.forEach((id) => {
				callback(this.items[id],id);
			})
		};
	}
	class HtmlItem {
		constructor(name, params = {}, element = false) {
			let self = this;
			let tag = params.tagName ? params.tagName : 'div';
			delete params.tagName;
			self.name = name;
			self.childCollection = new ItemCollection();
			self.element = element !== false ? element :  document.createElement(tag);
			if (element === false) {
				self.setParamRecursive(self.element, params);
			}
			self.Suggest = false;
			self.ShowCallback = (self) => {self.getElement().style.display = 'block';};
			self.HideCallback = (self) => {self.getElement().style.display = 'none';};
			self.data = {};
			self.PlaceMark = {};
			return this;
		}
		setCurrent = () => {
			this.getElement().classList.add('current')
		};
		removeCurrent = () => {
			this.getElement().classList.remove('current')
		};
		setOpen = () => {
			this.getElement().classList.add('open')
		};
		removeOpen = () => {
			this.getElement().classList.remove('open')
		};
		hide = () => {
			this.HideCallback(this);
		};
		show = () => {
			this.ShowCallback(this);
		};
		setData = Data => {this.data = Data};
		getData = () => this.data;
		/**
		 * @param (name {string})
		 * @param (HtmlItem {HtmlItem})
		 */
		replaceChild = (name, HtmlItem = false) => {
			let child = this.getChild(name);
			if (HtmlItem === false) {
				child.getElement().remove();
				this.childCollection.replace(name)
			} else {
				child.getElement().replaceWith(HtmlItem.getElement());
				this.childCollection.set(name, HtmlItem)
			}
		};
		/** @return HtmlItem**/
		getChild = name => this.childCollection.get(name);
		getChilds = () => this.childCollection;
		getName = () => this.name;
		getElement = () => this.element;
		/**
		 * @param (object {{'StyleName' => 'StyleValue'}}
		 */
		setCss = (object = {}) => {
			for (let styleName in object) {
				this.getElement().style[styleName] = object[styleName];
			}
		};
		/**
		 * @param eventName {string}
		 * @param callback {function}
		 */
		handleEvent = (eventName, callback) => {
			this.getElement()[eventName] = callback;
		};
		/**
		 * @param eventName {string}
		 * @param callback {function}
		 */
		addEventListener = (eventName, callback) => {
			this.getElement().addEventListener(eventName, callback);
		};
		setParamRecursive = (element, params) => {
			for (let key in params) {
				let value = params[key];
				if (Array.isArray(params[key]) && Array.isArray(value)) {
					for (let i in value) {
						element[key].add(value[i]);
					}
				} else if (Array.isArray(value) || (value !== null && typeof value === 'object')) {
					this.setParamRecursive(element[key], value);
				} else {
					element[key] = value;
				}
			}
		};
		/** @return HtmlItem **/
		addChildMultiple = childs => {
			for (let i in childs) {
				if (!childs[i]) {
					continue;
				}
				this.addChild(childs[i])
			}
			return this;
		};
		/** @return HtmlItem **/
		addChild = child => {
			if (!child) {
				return this;
			}
			let childCollection = this.getChilds();

			if (childCollection.has(child.getName())) {
				throw new Error('child Id duplicate');
			}
			this.element.appendChild(child.getElement());
			childCollection.add(child, child.getName());
			return this;
		};
		removeChilds = () => {
			let childCollection = this.getChilds();

			childCollection.each((item) => {
				item.getElement().remove();
			})
			delete childCollection.items;
			delete childCollection.ids
			childCollection.items = {};
			childCollection.ids = [];
			this.childCollection = childCollection;
			return this;
		};
		appendAsChild = element => {
			element.appendChild(this.getElement());
		};
	}
	class ParamsCollections {
		constructor(params, WidgetObject) {
			let self = this;
			self.WidgetObject = WidgetObject;
			if (!params.service_path) {
				throw new Error('`service_path` required');
			}

			if (params.search_on_start === true && !params.default_city) {
				throw new Error('`default_city` required for `search_on_start = true`');
			}

			if (!params.link) {
				throw new Error('`link` required');
			}
			if (params.only_delivery_type && !['Pvz', 'Courier'].includes(params.only_delivery_type)) {
				throw new Error('`only_delivery_type` required be `Pvz` or `Courier`');
			} else {
				self.onlyOneDeliveryType = !!params.only_delivery_type;
			}

			if (!params.yandex_map_api_key) {
				throw new Error('`yandex_map_api_key` required')
			}

			self.servicepath = params.service_path;
			self.defaultCity = params.default_city ? params.default_city : false;
			self.link = params.link;
			self.yandex_map_api_key = params.yandex_map_api_key;

			self.onlyCourier = params.only_delivery_type === 'Courier';
			self.onlyPvz = params.only_delivery_type === 'Pvz';

			self.onlyInfo = params.only_info === true;
			self.popup_mode = params.popup_mode === true;

			let handlersNames = [
				'onSelectCourierItem',
				'onSelectPvzItem',
				'onAjaxGetShippingMethods',
				'onAjaxGetPickPoints',
				'onPopupClose',
			];
			self.onSelectCourierItem = false;
			self.onSelectPvzItem = false;
			self.onAjaxGetShippingMethods = false;
			self.onAjaxGetPickPoints = false;
			self.onPopupClose = false;
			for (let i in handlersNames) {
				let name = handlersNames[i];
				if (typeof(params[name]) === 'function') {
					this[name] = params[name];
				}
			}
			const cargo_allow_params = ['weight', 'max_weight', 'max_size'];
			self.CargoData = {};
			for (let key in params.cargo) {
				if (!cargo_allow_params.includes(key)) {
					continue;
				}
				self.CargoData[key] = params.cargo[key];
			}


			self.defaultCityPostcode = params.default_city_postcode ? params.default_city_postcode : false;

			if (params.search_on_start === true) {
				self.searchWhenStart = params.search_on_start;
			}

			self.searchArea = false;
			if (params.search_area) {
				self.searchArea = params.search_area;
			}
		}
		getWeight = () => this.CargoData.weight > 0 ? this.CargoData.weight : 0;
		getMaxWeight = () => this.CargoData.max_weight > 0 ? this.CargoData.max_weight : 0;
		getMaxSize = () => this.CargoData.max_size > 0 ? this.CargoData.max_size : 0;
		isPopupMode = () => this.popup_mode;
		searchOnStat = () => this.searchWhenStart === true;
		hasSearchArea = () => Array.isArray(this.searchArea) && this.searchArea[0] > 0 && this.searchArea[1] > 0
		getSearchArea = () => this.searchArea;
		isOnlyInfo = () => this.onlyInfo;
		getServicePatch = () => this.servicepath;
		getDefaultCity = () => this.defaultCity;
		getWidgetId = () => this.link;
		getYandexMapApiKey = () => this.yandex_map_api_key;
		isCourierOnly = () => this.onlyCourier;
		isOnlyPvz = () => this.onlyPvz;

		hasHandlerOnPopupClose = () => this.onPopupClose !== false;
		getOnPopupCloseHandler = () => this.onPopupClose;

		hasHandlerOnSelectCourierItem = () => this.onSelectCourierItem !== false;
		getOnSelectCourierItemHandler = () => this.onSelectCourierItem;

		hasHandlerOnSelectPvzItem = () => this.onSelectPvzItem !== false;
		getOnSelectPvzItemHandler = () => this.onSelectPvzItem;

		getOnAjaxGetShippingMethods = () => this.onAjaxGetShippingMethods;
		hasOnAjaxGetShippingMethods = () => this.onAjaxGetShippingMethods !== false;

		getOnAjaxGetPickPoints = () => this.onAjaxGetPickPoints;
		hasOnAjaxGetPickPoints = () => this.onAjaxGetPickPoints !== false;
	}
	class AjaxEngine {
		constructor(WidgetObject) {
			let self = this;
			self.Params = WidgetObject.getParams();
			self.Widget = WidgetObject;
		}
		getWidget = () => this.Widget;
		getParams = () => this.Params;
		/**
		 * @returns {DataCollection}
		 */
		getData = () => this.Widget.getData();
		getShippingMethods = async (searchString) => {
			let data = new FormData();
			data.append('address', searchString);
			data.append('weight', this.getParams().getWeight());
			this.getWidget().getMap().removeBalloon();
			const ShippingMethodsResponse = await fetch(
				this.getParams().getServicePatch() + '?method=shippingMethods',
				{
					method: 'POST',
					body: data
				}
			);
			return await ShippingMethodsResponse.json();
		};
		getPvzList = async () => {
			let data = new FormData();
			data.append('postcode', this.getData().getPostcode());
			data.append('cityName', this.getData().getSelectedCity());


			if (this.getParams().getMaxWeight() > 0) {
				data.append('maxWeight', this.getParams().getMaxWeight());
			}
			if (this.getParams().getMaxSize() > 0) {
				data.append('maxSize', this.getParams().getMaxSize());
			}

			const PvzListRequest = await fetch(this.getParams().getServicePatch() + '?method=pvzList', {
				method: 'POST',
				body: data
			})
			return await PvzListRequest.json();
		};
	}

	class DataCollection {
		getCourierItems = () => this.CourierItems;
		resetCourierItems = () => {
			delete this.CourierItems;
			this.CourierItems = {};
		};
		addCourierItem = CourierItem => {
			this.CourierItems[CourierItem.getId()] = CourierItem;
		};
		getCourierItem = code => this.CourierItems[code];

		getPvzItems = () => this.PvzItems;
		resetPvzItems = () => {
			delete this.PvzItems;
			this.PvzItems = {};
		};
		addPvzItem = PvzItem => {this.PvzItems[PvzItem.getId()] = PvzItem;};
		getPvzItem = code => this.PvzItems[code];

		getFilterItems = () => this.FilterItems;
		resetFilterItems = () => {
			delete this.FilterItems;
			this.FilterItems = {};
		};
		addFilterItem = FilterItem => {this.FilterItems[FilterItem.getId()] = FilterItem;};
		getFilterItem = code => this.FilterItems[code];


		setSelectedCity = (CityData) => {this.SelectedCity = CityData};
		getSelectedCity = () => this.SelectedCity;
		setPostcode = (Postcode) => {this.Postcode = Postcode};
		getPostcode = () => this.Postcode;

		getShippingMethods = (key) => this.ShippingMethods[key];
		hasShippingMethods = (key) => this.ShippingMethods[key] !== null && this.ShippingMethods[key] !== undefined;
		setShippingMethods = (key, ShippingMethodsData) => {this.ShippingMethods[key] = ShippingMethodsData;};
		getShippingMethodByCode = (code) => this.ShippingMethodsByCode[code];
		resetShippingMethodsByCode = () => {this.ShippingMethodsByCode = {};};
		addShippingMethodByCode = (code, ShippingMethodsByCode) => {
			this.ShippingMethodsByCode[code] = ShippingMethodsByCode;
		};
		getAllShippingMethodByCode = () => this.ShippingMethodsByCode;
		hasPvzList = () => this.PvzLists[this.getPostcode()] !== null && this.PvzLists[this.getPostcode()] !== undefined;
		addPvzList = (PvzList) => {this.PvzLists[this.getPostcode()] = PvzList};
		getPvzList = () => this.PvzLists[this.getPostcode()];

		FilterItems = {};
		PvzLists = {};
		Postcode;
		SelectedCity;
		CourierItems;
		PvzItems;
		ShippingMethods = {};
		ShippingMethodsByCode = {};
		calkMinDatePrice = Items => {
			return {
				price: this.calkMin(Items, 'price'),
				date: this.calkMin(Items, 'date'),
			};
		};
		calkMin(Items = [], key = 'price') {
			let ItemsWithValue = Items.filter((Item) => {
				return Item[key] !== null;
			})
			let ItemsValue = ItemsWithValue.map((Item)=>{
				if (key === 'day') {
					const diff = Math.abs(new Date(Item[key]).getTime() - new Date().getTime());
					return Math.round(diff / (3600 * 1000 * 24));
				} else {
					return Item[key];
				}
			});

			return Math.min(...ItemsValue);
		}
	}
	class StateLoader{
		constructor(WidgetObject, MapCover) {
			let self = this;
			self.WidgetObject = WidgetObject;
			self.MapCover = MapCover;
			self.states = [];
			self.isReady = false;
		}
		load = onLoadCallback => {
			this.onLoadCallback = onLoadCallback;
			if (document.readyState !== 'complete') {
				addEventListener("DOMContentLoaded", this.onDocumentReady.bind(this));
			} else {
				this.onDocumentReady();
			}

			this.MapCover.onReady = () => {
				this.states.push('YandexMapApiReady');
				this.stateChanged();
			};
			this.MapCover.load();
		};
		onDocumentReady = () => {
			this.states.push('documentReady');
			this.stateChanged();
		};
		stateChanged = () => {
			if (!this.isReady
				&& this.states.includes('documentReady')
				&& this.states.includes('YandexMapApiReady')
			) {
				this.isReady = true;
				this.onLoadCallback();
			}
		};
	}
	class MapCover {
		constructor(WidgetObject, lang = 'ru') {
			let self = this;
			self.Map = false;
			self.lang = lang === 'ru' ? 'ru_RU' : 'en_GB';
			self.scriptObject = false;
			self.srcPatch = 'https://api-maps.yandex.ru/';
			self.baseVersion = '2.1.66';
			self.src = self.srcPatch + self.baseVersion + '/?lang=' + self.lang + '&ns=FmlYmapNamespace'
				+ '&apikey=' + WidgetObject.getParams().getYandexMapApiKey();
			self.Clusterer = false;
			self.WidgetObject = WidgetObject;
			self.baseIconLayout = false;
			self.selectedIconLayout = false;
			self.iconLayoutByOperator = {};
			self.SearchInput = false;
		}
		getWidgetObject = () => this.WidgetObject;
		getMap = () => this.Map;
		setMap = map => {
			this.Map = map;
			this.createClusterer();
			this.Map.geoObjects.add(this.getClusterer());

			if (!window.FMLogisticWidgetMaps) {
				window.FMLogisticWidgetMaps = {};
			}
			window.FMLogisticWidgetMaps[this.getWidgetObject().getParams().getWidgetId()] = map;
		};
		createSearchInput = (options) => {
			this.SearchInput = new FmlYmapNamespace.control.SearchControl({
				options: options
			});
			this.SearchInput.events.add('load', this.removeBalloon);
		}
		getSearchInput = () => this.SearchInput;
		setOperatorIcon = (code, src) => {
			this.iconLayoutByOperator[code] = [
				FmlYmapNamespace.templateLayoutFactory.createClass(
					'<div class="fml-widget-selected-placemark_point_icon"><img src="' + src + '"></div>'
				),
				FmlYmapNamespace.templateLayoutFactory.createClass(
					'<div class="fml-widget-selected-placemark_point_icon selected"><img src="' + src + '"></div>'
				)
			];
		};
		createClusterer = () => {
			this.Clusterer = new FmlYmapNamespace.Clusterer({
				preset: 'islands#invertedVioletClusterIcons',
				disableClickZoom: false,
				groupByCoordinates: false,
				clusterDisableClickZoom: false,
				maxZoom: 16,
				minClusterSize: 3,
			});
			this.baseIconLayout = FmlYmapNamespace.templateLayoutFactory.createClass(
				'<div class="fml-widget-selected-placemark"></div>'
			);
			this.selectedIconLayout = FmlYmapNamespace.templateLayoutFactory.createClass(
				'<div class="fml-widget-selected-placemark selected"></div>'
			);
		};
		centerOnPlaceMark = (PlaceMark) => {
			let Container = this.getWidgetObject().getContainer().getContainer();
			this.removePlaceMarksSelect();
			PlaceMark.options.set({iconLayout: PlaceMark.selectedIconLayout});
			PlaceMark.Selected = true;

			this.getMap().setZoom(16);
			this.getMap().setCenter(PlaceMark.geometry._coordinates);
			let pixelCenter = this.getMap().getGlobalPixelCenter(this.getMap().getCenter());
			pixelCenter = [
				pixelCenter[0] + Container.offsetWidth / 4,
				pixelCenter[1]
			];
			let geoCenter = this.getMap().options.get('projection').fromGlobalPixels(pixelCenter, this.getMap().getZoom());
			this.getMap().setCenter(geoCenter);
			this.removeBalloon();
		};
		recenterMap = async (zoom = 14) => {
			await this.getMap().setZoom(zoom);
			await this.resetCenter();
			let Container = this.getWidgetObject().getContainer().getContainer();
			let pixelCenter = this.getMap().getGlobalPixelCenter(this.getMap().getCenter());
			pixelCenter = [
				pixelCenter[0] + Container.offsetWidth / 4,
				pixelCenter[1]
			];
			let geoCenter = this.getMap().options.get('projection').fromGlobalPixels(pixelCenter, this.getMap().getZoom());
			await this.getMap().setCenter(geoCenter);
			this.removeBalloon();
		};
		resetCenter = async () => {
			await this.getMap().setCenter(this.getWidgetObject().getCenter());
		}
		removePlaceMarksSelect = () => {
			let PlaceMarks = this.getClusterer().getGeoObjects();
			for (let i in PlaceMarks) {
				let PlaceMark = PlaceMarks[i];
				PlaceMark.options.set({iconLayout: PlaceMark.baseIconLayout});
				PlaceMark.Selected = false;
			}
		};
		removeBalloon = () => {
			const api_version = this.getMap().container._element.classList.toString().replace('-map', '');
			const balloon_selector = 'ymaps.' + api_version + '-balloon-pane';
			const balloon_point_selector = 'ymaps.' + api_version + '-islets_icon-with-caption';

			let tryCount = 0;
			let countRemove = 0;
			const findBalloonInterval = setInterval(() => {
				let balloon = document.querySelector(balloon_selector);
				let balloon_point = document.querySelector(balloon_point_selector);
				if (balloon) {
					balloon.style.display = 'none';
					countRemove++;
				}
				if (balloon_point) {
					balloon_point.style.display = 'none';
					countRemove++;
				}
				if (tryCount > 300 || countRemove >= 2) {
					clearInterval(findBalloonInterval);
				}
				tryCount++;
			}, 50);
		};
		getBasePlaceMarkIconLayout = () => this.baseIconLayout;
		getSelectedPlaceMarkIconLayout = () => this.selectedIconLayout;
		getClusterer = () => this.Clusterer;
		createPlaceMark = (TerminalData, openCallback = () => {}) => {
			let baseIconLayout = this.getBasePlaceMarkIconLayout();
			let selectedIconLayout = this.getSelectedPlaceMarkIconLayout();

			let PlaceMark = new FmlYmapNamespace.Placemark(TerminalData.coords, {
				hintContent: TerminalData.name,
			}, {
				iconLayout: baseIconLayout,
				iconImageSize: [40, 43],
				iconImageOffset: [0, 0],
				iconShape: {
					type: 'Rectangle',
					coordinates: [
						[-20, -40], [20, 3]
					]
				}
			});
			PlaceMark.selectedIconLayout = selectedIconLayout;
			PlaceMark.baseIconLayout = baseIconLayout;

			PlaceMark.events.add('click', openCallback);
			PlaceMark.events.add('mouseenter', function (e) {
				PlaceMark.options.set({iconLayout: selectedIconLayout});
			}).add('mouseleave', function (e) {
				if (PlaceMark.Selected !== true) {
					PlaceMark.options.set({iconLayout: baseIconLayout});
				}
			});
			return PlaceMark;
		};
		getCenter = () => this.getMap().getCenter();
		getScriptObject = () => {
			if (this.scriptObject) {
				return this.scriptObject;
			}
			let scriptsList = document.querySelectorAll('script');
			for (let i in scriptsList) {
				let src = scriptsList[i].src;
				if (src && src.includes('ns=FmlYmapNamespace')) {
					this.scriptObject = scriptsList[i];
					break;
				}
			}
			return this.scriptObject;
		};
		isLoaded = () => typeof this.getScriptObject() === 'object';
		checkVersion = (minVersion = this.baseVersion) => {
			let currentVersion = this.getVersion();
			if (currentVersion === false) {
				return Error('Yandex Map not included');
			}
			if (this.getVersion() < this.baseVersion) {
				throw new Error('Current Yandex Map version "'
					+ currentVersion
					+ '" is lower than "'
					+ minVersion
					+ '" can be some trouble'
				);
			}
		};
		checkYandexMapApiKey = () => {
			let scriptObject = this.getScriptObject();
			let matchResult = scriptObject.src.match(/\apikey=([\w\d\-]{0,})/i);
			if (!matchResult[1]) {
				throw new Error('Yandex Map need apikey for geocoder');
			}
		};
		loadChecker = callback => {
			let counterCheckYmapApiLoad = 0;
			let checkerYmapApiLoad = setInterval(function () {
				counterCheckYmapApiLoad++;
				if (counterCheckYmapApiLoad >= 50) {
					clearInterval(checkerYmapApiLoad);
					return false;
				}
				try {
					if (FmlYmapNamespace && typeof(FmlYmapNamespace.geocode) === 'function') {
						clearInterval(checkerYmapApiLoad);
						callback(FmlYmapNamespace);
					}
				} catch (e) {
				}
			}, 500);
		};
		loadScript = () => {
			let loadedTag = document.createElement('script');
			loadedTag.src = this.src;
			let head = document.getElementsByTagName('head')[0];
			head.appendChild(loadedTag);
			loadedTag.onload = this.scriptLoad.bind(this);
		};
		load = () => {
			if (!this.isLoaded()) {
				this.loadScript(this.scriptLoad.bind(this));
			} else {
				this.scriptLoad();
			}
		};
		scriptLoad = () => {
			this.loadChecker((FmlYmapNamespace) => {
				FmlYmapNamespace.ready(()=>{
					this.onReady();
				})
			});
		};
		onReady = () => {};
	}


	class BaseStruct {
		/**
		 * @type {HtmlItem}
		 */
		HtmlItem;
		constructor(Widget, Data = {}) {
			let self = this;
			self.Widget = Widget;
			self.Data = Data;
		}
		getWidget = () => this.Widget;
		getElement = () => this.getHtmlItem().getElement();
		/**
		 * @return {HtmlItem}
		 */
		getHtmlItem = () => this.HtmlItem;
		setData = (Data) => {this.Data = Data};
		getData = () => this.Data;
		createLogoHtmlItem = logoSrc => {
			let logoParams = {
				name: 'logo',
				tagName: 'img',
				src: logoSrc,
				alt: 'logo'
			};
			if (!logoSrc) {
				logoParams = {
					name: 'Logo',
					classList: ['fml-widget__panel-content__default_logo'],
					alt: 'logo'
				};
			}
			return new HtmlItem('Logo', logoParams);
		};
	}
	class DeliverySelector extends BaseStruct{
		constructor(WidgetObject) {
			super(WidgetObject);
			let self = this;

			self.HtmlItem = (new HtmlItem('Selector', {
				classList: ['fml-widget__delivery-type'],
			})).addChildMultiple([
				(new HtmlItem('SelectorContainer', {
					classList: ['fml-widget__delivery-container'],
				})).addChildMultiple([
					(new HtmlItem('TitleContainer', {
						classList: ['fml-widget__primary-title'],
					})).addChild(new HtmlItem('Title', {
						tagName: 'span',
						classList: ['fml_choose'],
						innerHTML: Utility.getSelectTypeTitle(),
					})),
					(new HtmlItem('VariantsSelector', {
						classList: ['fml-widget__delivery-type__options', 'fml-widget__tab-js'],
					}))
				]),
				(new HtmlItem('ListContainer', {
					classList: [
						'fml-widget__delivery-type__options-content',
						'fml-widget__content-js',
						'fml-widget__scroll'
					],
				})).addChild(new HtmlItem('CourierList'), {
					classList: ['fml-widget__delivery-type__option-item', 'current'],
				}),
			]);
			if (self.getWidget().getParams().isOnlyPvz() !== true) {
				/**
				 * @type {HtmlItem}
				 */
				self.CourierVariantItem = (new HtmlItem('CourierVariantItem', {
					classList: ['fml-widget__delivery-type__item', 'current'],
				})).addChildMultiple([
					(new HtmlItem('TitleContainer', {
						classList: ['fml-widget__delivery-type__item-title'],
					})).addChild(new HtmlItem('Title', {
						tagName: 'span',
						innerHTML: Utility.getCourierVariantTitle(),
					})),
					(new HtmlItem('DetailContainer', {
						classList: ['fml-widget__delivery-type__item-details'],
					})).addChildMultiple([
						new HtmlItem('Price', {
							classList: ['fml-widget__price'],
						}),
						new HtmlItem('Time', {
							classList: ['fml-widget__days'],
						})
					]),
				]);
				this.getVariantTypeList().addChild(self.getCourierVariant());
			}
			if (self.getWidget().getParams().isCourierOnly() !== true) {
				/**
				 * @type {HtmlItem}
				 */
				self.PvzVariantItem = (new HtmlItem('PvzVariantItem', {
					classList: ['fml-widget__delivery-type__item'],
				})).addChildMultiple([
					(new HtmlItem('TitleContainer', {
						classList: ['fml-widget__delivery-type__item-title'],
					})).addChild(new HtmlItem('Title', {
						tagName: 'span',
						innerHTML: Utility.getPvzVariantTitle(),
					})),
					(new HtmlItem('DetailContainer', {
						classList: ['fml-widget__delivery-type__item-details'],
					})).addChildMultiple([
						new HtmlItem('Price', {
							classList: ['fml-widget__price'],
						}),
						new HtmlItem('Time', {
							classList: ['fml-widget__days'],
						})
					]),
				]);
				if (self.getWidget().getParams().isOnlyPvz()) {
					self.getPvzVariant().getElement().classList.add('current')
				}
				this.getVariantTypeList().addChild(self.getPvzVariant());
			}
			self.isOpen = false;
		}
		getVariantTypeList = () => this.getHtmlItem().getChild('SelectorContainer').getChild('VariantsSelector');
		getCourierVariant = () => this.CourierVariantItem;
		getPvzVariant = () => this.PvzVariantItem;
		getCourierItemList = () => this.getHtmlItem().getChild('ListContainer').getChild('CourierList');
		removeCourierItems = () => {
			this.getCourierItemList().removeChilds();
		}
		addCourierItem = CourierItem => {
			this.getCourierItemList().addChild(CourierItem.getHtmlItem());
		};
		/**
		 * @param Variant {{date: int, price: int}}
		 */
		setCourierTypeVariantData = (Variant) => {
			this.getCourierVariant().getChild('DetailContainer')
				.getChild('Price').getElement().innerHTML = Utility.getPriceFrom(Variant.price);
			this.getCourierVariant().getChild('DetailContainer')
				.getChild('Time').getElement().innerHTML = Utility.getWorkDay(Variant.date);
		}
		/**
		 * @param Variant {{date: int, price: int}}
		 */
		setPvzTypeVariantData = (Variant) => {
			this.getPvzVariant().getChild('DetailContainer')
				.getChild('Price').getElement().innerHTML = Utility.getPriceFrom(Variant.price);
			this.getPvzVariant().getChild('DetailContainer')
				.getChild('Time').getElement().innerHTML = Utility.getWorkDay(Variant.date);
		}
		open = () => {
			this.getHtmlItem().setCurrent();
			this.isOpen = true;
		};
		close = () => {
			this.getHtmlItem().removeCurrent();
			this.isOpen = false;
		};
	}
	class DeliveryCourierVariantItem extends BaseStruct {
		/**
		 * @param ItemParams {{contractor_id: int, price: int, days_min: int, name: string, tariff_name: string, logo_small: string }}
		 * @param WidgetObject
		 */
		constructor(ItemParams, WidgetObject) {
			super(WidgetObject, ItemParams);

			let self = this;
			let TimeStr = Utility.getVariantDay(ItemParams.days_min);

			let Name = ItemParams.name;
			if (TimeStr) {
				Name += ',';
			}

			let price = Utility.getPrice(ItemParams.price);

			let deliveryType = ItemParams.tariff_name.slice(0,32);

			if (ItemParams.tariff_name.length > 32) {
				deliveryType += '...';
			}

			self.HtmlItem = (new HtmlItem(this.getId(), {
				classList: ['fml-widget__delivery-type__list-item'],
			})).addChildMultiple([
				(new HtmlItem('LogoContainer', {
					classList: ['fml-widget__delivery-type__logo'],
				})).addChild(this.createLogoHtmlItem(ItemParams.logo_small)),
				(new HtmlItem('InfoContainer', {
					classList: ['fml-widget__delivery-type__info'],
				})).addChildMultiple([
					(new HtmlItem('Date', {
						classList: ['fml-widget__delivery-type__info-date'],
					})).addChildMultiple([
						new HtmlItem('Name', {
							tagName: 'span',
							innerHTML: Name
						}),
						new HtmlItem('Time', {
							tagName: 'span',
							innerHTML: TimeStr
						}),
					]),
					(new HtmlItem('Type', {
						classList: ['fml-widget__delivery-type__info-type'],
						innerHTML: deliveryType,
					})),
				]),
				(new HtmlItem('PriceContainer', {
					classList: ['fml-widget__delivery-type__price-wrap'],
				})).addChildMultiple([
					(new HtmlItem('Price', {
						classList: ['fml-widget__delivery-type__price'],
						innerHTML: price,
					})),
					self.getWidget().getParams().isOnlyInfo() ? (new HtmlItem('Select')) : (new HtmlItem('Select', {
						classList: ['fml-widget__button'],
						innerHTML: Utility.getSelectText(),
					}))
				]),
			]);
			if (self.getWidget().getParams().isOnlyInfo() !== true) {
				self.getHtmlItem().handleEvent('onclick', (e) => {
					if (self.getWidget().getParams().hasHandlerOnSelectCourierItem()) {
						self.getWidget().getParams().getOnSelectCourierItemHandler()(self, self.getWidget());
					}
				})
			}

		}
		getId = () => 'CourierItem_' + this.getData().contractor_id;
	}
	class DeliverySidebar extends BaseStruct{
		constructor(WidgetObject) {
			super(WidgetObject);
			let self = this;

			let ButtonBuckData = new HtmlItem('BuckContainer');
			if (self.getWidget().getParams().isOnlyPvz() !== true) {
				ButtonBuckData = (new HtmlItem('BuckContainer', {
					classList: ['fml-widget__sidebar-button-back-wrap'],
				})).addChildMultiple([
					new HtmlItem('ButtonBuck', {
						classList: [
							'fml-widget__sidebar-button',
							'fml-widget__sidebar-button-back',
						],
						innerHTML: Utility.getSvgEnum().ButtonBack
					}),
					new HtmlItem('ButtonBackDesc', {
						classList: ['fml-widget__sidebar-button-back-text'],
						innerHTML: Utility.getButtonBuckDesc(),
					}),
				]);
			}

			self.HtmlItem = (new HtmlItem('DeliverySelector', {
				classList: ['fml-widget__sidebar'],
			})).addChildMultiple([
				(new HtmlItem('ButtonContainer', {
					classList: ['fml-widget__sidebar-button-wrap'],
				})).addChildMultiple([
					(new HtmlItem('Point', {
						classList: [
							'fml-widget__sidebar-button',
							'fml-widget__sidebar-burger',
							'fml-widget__sidebar-button-js',
						],
						innerHTML: Utility.getSvgEnum().Point,
					})),
					(new HtmlItem('Payment', {
						classList: [
							'fml-widget__sidebar-button',
							'fml-widget__sidebar-button-cash',
							'fml-widget__sidebar-button-checked',
						],
					})).addChildMultiple([
						new HtmlItem('All', {
							innerHTML: Utility.getSvgEnum().Payment_all
						}),
						new HtmlItem('Card', {
							style: {display: 'none'},
							innerHTML: Utility.getSvgEnum().Payment_card
						}),
						new HtmlItem('Cash', {
							style: {display: 'none'},
							innerHTML: Utility.getSvgEnum().Payment_cash
						}),
					]),
					// (new HtmlItem('Clothes', {
					// 	classList: [
					// 		'fml-widget__sidebar-button',
					// 		'fml-widget__sidebar-button-cal',
					// 		'fml-widget__sidebar-button-checked',
					// 		'fml-widget__sidebar-button-clothes',
					// 	],
					// })),
					(new HtmlItem('Filter', {
						classList: [
							'fml-widget__sidebar-button',
							'fml-widget__sidebar-button-delivery',
							'fml-widget__sidebar-button-js',
						],
					})).addChildMultiple([
						new HtmlItem('IconBaseContainer', {
							innerHTML: Utility.getSvgEnum().Filter
						}),
						new HtmlItem('IconActiveContainer', {
							tagName: 'span',
							classList: ['fml-widget__sidebar-button-setting-icon'],
							innerHTML: Utility.getSvgEnum().FilterActive
						}),
					])
				]),
				ButtonBuckData
			]);
			self.setData({
				isOpen: false,
			});

			const SidebarButtonsPlaceholders = [
				['Point', self.getWidget().getContainer().getPlaceholders().getForPvzList()],
				['Payment', self.getWidget().getContainer().getPlaceholders().getForPayment()],
				['Filter', self.getWidget().getContainer().getPlaceholders().getForFilterList()]
			];
			const isMobile = self.getWidget().isMobile();

			SidebarButtonsPlaceholders.forEach((Item) => {
				let Placeholder = Item[1];
				/**
				 * @type HtmlItem
				 */
				let SidebarButton = self.getHtmlItem().getChild('ButtonContainer').getChild(Item[0]);

				if (isMobile) {
					let interValForPlaceholder;
					SidebarButton.addEventListener('click', (e) => {
						Placeholder.getElement().classList.add('show');
						clearTimeout(interValForPlaceholder);
						interValForPlaceholder = setTimeout(() => {
							Placeholder.getElement().classList.remove('show');
						}, 3000);
					})
				} else {
					SidebarButton.handleEvent('onmouseover', () => {
						Placeholder.getElement().classList.add('show');
					});
					SidebarButton.handleEvent('onmouseout', () => {
						Placeholder.getElement().classList.remove('show');
					});
				}
			});
		}
		getButtons = () => this.getHtmlItem().getChild('ButtonContainer');
		getButtonPoint = () => this.getButtons().getChild('Point');
		getButtonPayment = () => this.getButtons().getChild('Payment');
		getButtonFilter = () => this.getButtons().getChild('Filter');
		getButtonBuck = () => this.getHtmlItem().getChild('BuckContainer').getChild('ButtonBuck');
		setPanelInfoPayTitle = (code = 'all') => {
			let AssetsPanelInfo = Utility.getPanelAssets();
			let newTitle = '';
			switch (code) {
				case 'card':
					newTitle = AssetsPanelInfo.getFilterPayCard();
					break;
				case 'cache':
					newTitle = AssetsPanelInfo.getFilterPayCache();
					break;
				case "all":
				default:
					newTitle = AssetsPanelInfo.getFilterPayAll();
					break;
			}
			this.getWidget().getContainer().getPlaceholders().getForPayment().getElement().innerHTML = newTitle;
		}
		open = () => {
			this.getHtmlItem().setCurrent();
			this.getData().isOpen = true;
			this.isOpen = true;
		};
		close = () => {
			this.getHtmlItem().removeCurrent();
			this.getData().isOpen = false;
		};
	}
	class DetailFrame extends BaseStruct {
		constructor(Widget) {
			super(Widget);
			let self = this;
			self.HtmlItem = (new HtmlItem('Detail', {
				classList: ['fml-widget__panel-details'],
			})).addChildMultiple([
				(new HtmlItem('TitleContainer', {
					classList: ['fml-widget__primary-title'],
				})).addChildMultiple([
					(new HtmlItem('Buck', {
						classList: ['fml-widget__panel-details__back'],
						innerHTML: Utility.getSvgEnum().DetailBuck,
					})),
					(new HtmlItem('Title', {
						tagName: 'span',
					})),
				]),
				(new HtmlItem('DetailContainer', {
					classList: ['fml-widget__panel-details__list', 'fml-widget__scroll'],
				})).addChild((new HtmlItem('ContainerInner', {
					classList: ['fml-widget__panel-details__item'],
				})).addChildMultiple([
					(new HtmlItem('Header', {
						classList: ['fml-widget__panel-details__item-header'],
					})).addChildMultiple([
						(new HtmlItem('LogoContainer', {
							classList: ['fml-widget__panel-details__logo'],
						})),
						(new HtmlItem('Info', {
							classList: ['fml-widget__panel-details__info'],
						})).addChildMultiple([
							new HtmlItem('Address', {
								tagName: 'span',
								classList: ['fml-widget__panel-details__info-amount'],
							}),
							new HtmlItem('Time', {
								tagName: 'span',
								classList: ['fml-widget__panel-details__info-delivery'],
							}),
						]),
					]),
					(new HtmlItem('Body', {
						classList: ['fml-widget__panel-details__item-content'],
					})).addChildMultiple([
						(new HtmlItem('Price', {
							classList: ['fml-widget__panel-details__price-wrap'],
						})).addChildMultiple([
							new HtmlItem('Title', {
								classList: ['fml-widget__panel-details__title'],
								innerHTML: Utility.getDetailAssets().getPriceTitle(),
							}),
							new HtmlItem('Price', {
								classList: ['fml-widget__panel-details__price'],
							}),
							self.getWidget().getParams().isOnlyInfo() ? new HtmlItem('Select') : new HtmlItem('Select', {
								classList: ['fml-widget__button'],
								innerHTML: Utility.getDetailAssets().getSelectTitle(),
							})
						]),
						(new HtmlItem('Address', {
							classList: ['fml-widget__panel-details__address-wrap'],
						})).addChildMultiple([
							new HtmlItem('Title', {
								classList: ['fml-widget__panel-details__title'],
								innerHTML: Utility.getDetailAssets().getAddressTitle(),
							}),
							(new HtmlItem('ValueContainer', {
								classList: ['fml-widget__panel-details__info-wrap'],
							})).addChild(new HtmlItem('Value', {
								tagName: 'span',
							})),
						]),
						(new HtmlItem('WorkTime', {
							classList: ['fml-widget__panel-details__working-hours-wrap'],
						})).addChildMultiple([
							new HtmlItem('Title', {
								classList: ['fml-widget__panel-details__title'],
								innerHTML: Utility.getDetailAssets().getWorkTimeTitle(),
							}),
							(new HtmlItem('Value', {
								classList: ['fml-widget__panel-details__info-wrap'],
							})).addChild(new HtmlItem('ValueSpan', {
								tagName: 'span',
							})),
						]),
						(new HtmlItem('Phone', {
							classList: ['fml-widget__panel-details__phones-wrap'],
						})).addChildMultiple([
							new HtmlItem('Title', {
								classList: ['fml-widget__panel-details__title'],
								innerHTML: Utility.getDetailAssets().getPhoneTitle(),
							}),
							new HtmlItem('Value', {
								classList: ['fml-widget__panel-details__info-wrap'],
							}),
						]),
						(new HtmlItem('Description', {
							classList: ['fml-widget__panel-details__description-wrap'],
						})).addChildMultiple([
							new HtmlItem('Title', {
								classList: ['fml-widget__panel-details__title'],
								innerHTML: Utility.getDetailAssets().getDescTitle(),
							}),
							(new HtmlItem('ValueContainer', {
								classList: ['fml-widget__panel-details__info-wrap'],
							})).addChild(new HtmlItem('Value', {
								tagName: 'span'
							})),
						]),
					]),
				])),
			]);
			self.setData({});
			if (self.getWidget().getParams().isOnlyInfo() !== true) {
				self.getHtmlItem().getChild('DetailContainer')
					.getChild('ContainerInner')
					.getChild('Body')
					.getChild('Price')
					.getChild('Select')
					.handleEvent('onclick', e => {
						if (self.getWidget().getParams().hasHandlerOnSelectPvzItem()) {
							self.getWidget().getParams().getOnSelectPvzItemHandler()(self.getData(), self.getWidget());
						}
					});
			}

			self.getHtmlItem().getChild('TitleContainer').getChild('Buck').handleEvent('onclick',e => {
				self.getHtmlItem().removeCurrent();
				let Panel = self.getWidget().getContainer().getDeliveryPanel();
				let PvzList = Panel.getHtmlItem().getChild('Containers').getChild('PvzListContainer');

				PvzList.show();
				Panel.getPvzContainer().show();
				Panel.getHtmlItem().getChild('Containers').removeCurrent();
			});
		}
		setItem = ItemPvz => {
			this.setData(ItemPvz);
			const Data = ItemPvz.getData();

			let TitleContainer = this.getHtmlItem().getChild('TitleContainer');
			let Detail = this.getHtmlItem().getChild('DetailContainer').getChild('ContainerInner');
			let Header = Detail.getChild('Header');
			let Logo = Header.getChild('LogoContainer');
			let HeaderAddress = Header.getChild('Info').getChild('Address');
			let Time = Header.getChild('Info').getChild('Time');
			let Body = Detail.getChild('Body');
			let PriceContainer = Body.getChild('Price');
			let AddressContainer = Body.getChild('Address');
			let WorkTimeContainer = Body.getChild('WorkTime');
			let PhoneContainer = Body.getChild('Phone');
			let DescriptionContainer = Body.getChild('Description');

			let ShippingMethod = Data.Method;
			if (ShippingMethod.days_min > 0 || ShippingMethod.days_min === 0) {
				Time.show();
				Time.getElement().innerHTML = Utility.getWorkDay(ShippingMethod.days_min);
			} else {
				Time.getElement().innerHTML = '';
				Time.hide();
			}

			if (ShippingMethod.price > 0 || ShippingMethod.price === 0) {
				PriceContainer.show();
				PriceContainer.getChild('Price').getElement().innerHTML = Utility.getPrice(ShippingMethod.price);
			} else {
				PriceContainer.hide();
				PriceContainer.getChild('Price').getElement().innerHTML = '';
			}
			AddressContainer.show();
			if (Data.fullAddress && Data.shortAddress) {
				AddressContainer.getChild('ValueContainer').getChild('Value').getElement().innerHTML = Data.fullAddress;
				TitleContainer.getChild('Title').getElement().innerHTML = Data.shortAddress;
				HeaderAddress.getElement().innerHTML = Data.shortAddress;
			} else if (Data.shortAddress && !Data.fullAddress) {
				AddressContainer.getChild('ValueContainer').getChild('Value').getElement().innerHTML = Data.shortAddress;
				TitleContainer.getChild('Title').getElement().innerHTML = Data.shortAddress;
				HeaderAddress.getElement().innerHTML = DatashortAddress;
			} else if (!Data.shortAddress && Data.fullAddress) {
				AddressContainer.getChild('ValueContainer').getChild('Value').getElement().innerHTML = Data.fullAddress;
				HeaderAddress.getElement().innerHTML = Data.fullAddress;
				TitleContainer.getChild('Title').getElement().innerHTML = Data.fullAddress;
			} else {
				AddressContainer.hide();
				AddressContainer.getChild('ValueContainer').getChild('Value').getElement().innerHTML = '';
				HeaderAddress.getElement().innerHTML = '';
				TitleContainer.getChild('Title').getElement().innerHTML = '';
			}

			if (Data.openingHoursText) {
				WorkTimeContainer.show();
				WorkTimeContainer.getChild('Value').getChild('ValueSpan').getElement().innerHTML = Data.openingHoursText;
			} else {
				WorkTimeContainer.hide();
				WorkTimeContainer.getChild('Value').getChild('ValueSpan').getElement().innerHTML = '';
			}

			if (Data.phoneNumber) {
				PhoneContainer.show();
				PhoneContainer.getChild('Value').getElement().innerHTML = Data.phoneNumber;
				PhoneContainer.getChild('Value').addChild(new HtmlItem('PhoneValue', {
					tagName: 'a',
					classList: ['fml-widget__panel-details__info-phone'],
					innerHTML: Data.phoneNumber,
					href: 'tel:' + Data.phoneNumber,
				}));
			} else {
				PhoneContainer.getChild('Value').removeChilds();
				PhoneContainer.hide();
			}

			if (Data.additionalDescription) {
				DescriptionContainer.show();
				DescriptionContainer.getChild('ValueContainer')
					.getChild('Value').getElement().innerHTML = Data.additionalDescription;
			} else {
				DescriptionContainer.getChild('ValueContainer').getChild('Value').innerHTML = '';
				DescriptionContainer.hide();
			}

			if (ShippingMethod.logo_small) {
				Logo.getElement().innerHTML = this.getWidget().makeLogoParams(ShippingMethod.logo_small).getElement().outerHTML;
			} else {
				Logo.getElement().innerHTML = '';
			}
		}
	}
	class DeliveryPanel extends BaseStruct{
		constructor(WidgetObject, count = 0) {
			super(WidgetObject);
			let self = this;
			self.DetailFrame = new DetailFrame(self.getWidget());
			self.HtmlItem = (new HtmlItem('DeliveryPanel', {
				classList: ['fml-widget__panel'],
			})).addChildMultiple([
				(new HtmlItem('Containers', {
					classList: ['fml-widget__panel-list'],
				})).addChildMultiple([
					(new HtmlItem('Buck', {
						classList: ['fml-widget__panel-details__back'],
						innerHTML: Utility.getSvgEnum().DetailBuck,
					})),
					(new HtmlItem('PvzListContainer', {
						classList: ['fml-widget__panel-content-wrap', 'fml-widget__panel-list-points'],
					})).addChildMultiple([
						(new HtmlItem('Title', {
							classList: ['fml-widget__panel-headline', 'fml-widget__primary-title'],
							innerHTML: Utility.getPvzListTitle(''),
						})),
						(new HtmlItem('List', {
							classList: ['fml-widget__panel-content', 'fml-widget__scroll']
						}))
					]),
					(new HtmlItem('FilterContainer', {
						classList: ['fml-widget__panel-content-wrap', 'fml-widget__panel-list-delivery'],
					})).addChildMultiple([
						(new HtmlItem('Title', {
							classList: ['fml-widget__panel-headline', 'fml-widget__primary-title'],
							innerHTML: Utility.getFilterListTitle(),
						})),
						(new HtmlItem('List', {
							classList: ['fml-widget__panel-content', 'fml-widget__scroll'],
						}))
					]),
				]),
				self.getDetail().getHtmlItem(),
			]);

			self.setData({
				isOpen: false,
				isPvzOpen: false,
				isFilterOpen: false,
			});
		}
		getDetail = () => this.DetailFrame;
		open = () => {
			this.getData().isOpen = true;
			this.getHtmlItem().setOpen();
			this.getWidget().getContainer().getDeliverySidebar().getButtonPoint().setCurrent();
		};
		close = (zoom = true) => {
			this.getHtmlItem().removeOpen();
			this.getData().isOpen = false;
			this.getWidget().getContainer().getDeliverySidebar().getButtonPoint().removeCurrent();
		};

		getPvzContainer = () => this.getHtmlItem().getChild('Containers').getChild('PvzListContainer').getChild('List');
		getFilterContainer = () => this.getHtmlItem().getChild('Containers').getChild('FilterContainer').getChild('List');
		addPvzPoint = Item => {
			this.getPvzContainer().addChild(Item.getHtmlItem());
		};
		addFilterItem = Item => {
			this.getFilterContainer().addChild(Item.getHtmlItem());
		};
		setPointTitleCount = count => {
			this.getHtmlItem().getChild('Containers').getChild('PvzListContainer').getChild('Title')
				.getElement().innerHTML = Utility.getPvzListTitle(count);
		};
		openDetail = ItemPvz => {
			this.getDetail().setItem(ItemPvz);
			this.getDetail().getHtmlItem().setCurrent();

			let Structure = this.getWidget().getContainer();
			let Sidebar = Structure.getDeliverySidebar();
			Sidebar.getButtonPoint().removeCurrent();
			Sidebar.getButtonFilter().removeCurrent();
			this.getFilterContainer().hide();
			this.getPvzContainer().show();
			this.getHtmlItem().setOpen();
			this.getHtmlItem().getChild('Containers').setCurrent();
			this.getDetail().getHtmlItem().setCurrent();
			Sidebar.getButtonPoint().setCurrent();
			if (window.innerWidth <= 767) {
				Sidebar.close();
			}
			this.getWidget().getMap().centerOnPlaceMark(ItemPvz.getPlaceMark());
		};

	}

	class DeliveryPvzItem  extends BaseStruct{
		constructor(ItemParams, WidgetObject) {
			super(WidgetObject);
			let self = this;
			self.setData(ItemParams);

			let ShippingMethod = self.getWidget().getData().getShippingMethodByCode(ItemParams.lms)

			self.HtmlItem = (new HtmlItem(self.getId(), {
				classList: ['fml-widget__panel-content_list-item'],
			})).addChildMultiple([
				(new HtmlItem('Logo', {
					classList: ['fml-widget__panel-content__logo'],
				})).addChild(self.getWidget().makeLogoParams(ShippingMethod.logo_small)),
				(new HtmlItem('Info', {
					classList: ['fml-widget__panel-content__info'],
				})).addChildMultiple([
					new HtmlItem('Address', {
						tagName: 'span',
						classList: ['fml-widget__panel-content__info-address'],
						innerHTML: self.getData().shortAddress
					}),
					new HtmlItem('PriceDate', {
						tagName: 'span',
						classList: ['fml-widget__panel-content__info-delivery'],
						innerHTML: Utility.getPvzItemInfo(ShippingMethod),
					})
				]),
			]);
			self.getData().isVisible = true;
			self.getData().isVisibleByType = true;
			self.getData().isVisibleByOperator = true;
			self.getData().wasVisible = true;
			self.getData().isVisibleByPay = true;
		}
		getId = () => {
			return 'PvzItem' + this.getData().pointId;
		}
		setPlaceMark = PlaceMark => {
			this.getData().PlaceMark = PlaceMark;
		};
		getPlaceMark = () => this.getData().PlaceMark;
	}
	class DeliveryPvzFilterItem extends BaseStruct{
		constructor(ItemParams, WidgetObject) {
			super(WidgetObject);
			let self = this;
			self.setData(ItemParams);

			let name = ItemParams.name ? ItemParams.name : ItemParams.code;

			let priceAndTime = '';

			let dayText = Utility.getFilterDateFrom(ItemParams.days_min);
			let priceStr = Utility.getPrice(ItemParams.price);

			if (priceStr && dayText) {
				priceAndTime = priceStr + ', ' + dayText;
			} else if (priceStr && !dayText) {
				priceAndTime = priceStr;
			} else if (!priceStr && dayText) {
				priceAndTime = dayText;
			}

			self.HtmlItem = (new HtmlItem(self.getId(), {
				classList: ['fml-widget__panel-content_list-item'],
			})).addChildMultiple([
				(new HtmlItem('ItemStatusContainer', {
					classList: ['fml-widget__panel-content__status'],
				})).addChild(new HtmlItem('Status', {
					tagName: 'span',
					classList: ['fml-widget__panel-content__status-icon'],
				})),
				(new HtmlItem('ContainerLogo', {
					classList: ['fml-widget__panel-content__logo'],
				})).addChild(self.getWidget().makeLogoParams(self.getData().logo_small)),
				(new HtmlItem('ContainerInfo', {
					classList: ['fml-widget__panel-content__info'],
				})).addChildMultiple([
					(new HtmlItem('NameCount', {
						classList: ['fml-widget__panel-content__info-amount'],
					})).addChildMultiple([
						new HtmlItem('Name', {
							tagName: 'span',
							innerHTML: name,
						}),
						new HtmlItem('Count', {
							tagName: 'span',
							innerHTML: Utility.getFilterItemCount(),
						}),
					]),
					new HtmlItem('InfoDelivery', {
						classList: ['fml-widget__panel-content__info-delivery'],
						innerHTML: priceAndTime,
					})
				]),
			]);
		}
		getId = () => {
			return 'FilterItem_' + this.getData().Id;
		};
		activate = () => {
			this.getHtmlItem().getElement().classList.remove('fml-widget__disabled');
			this.getData().isActive = true;
		};
		disable = () => {
			this.getHtmlItem().getElement().classList.add('fml-widget__disabled');
			this.getData().isActive = false;
		};
	}


	class HtmlContainer {
		Widget;
		Container;
		Structure;
		constructor(Widget) {
			let self = this;
			self.Widget = Widget;

			self.Structure = {
				/** @type {HtmlItem} */
				Loader: null,
				/** @type {HtmlItem} */
				MapContainer: null,
				/** @type {HtmlItem} */
				FilterLoader: null,
				/** @type {DeliverySelector} */
				DeliverySelector: null,
				/** @type {DeliverySidebar} */
				DeliverySidebar: null,
				/** @type {DeliveryPanel} */
				DeliveryPanel: null,
				Placeholders: {
					/** @type {HtmlItem} */
					InputPlaceholder: null,
					/** @type {HtmlItem} */
					PvzListPlaceholder: null,
					/** @type {HtmlItem} */
					FilterPlaceholder: null,
					/** @type {HtmlItem} */
					FilterListPlaceholder: null,
					/** @return {HtmlItem} */
					getForInput: () => this.Structure.Placeholders.InputPlaceholder,
					/** @return {HtmlItem} */
					getForPvzList: () => this.Structure.Placeholders.PvzListPlaceholder,
					/** @return {HtmlItem} */
					getForPayment: () => this.Structure.Placeholders.FilterPlaceholder,
					/** @return {HtmlItem} */
					getForFilterList: () => this.Structure.Placeholders.FilterListPlaceholder,
				},
			};
		}
		createStructure = () => {
			/**
			 * Container
			 */
			{
				this.Container = document.getElementById(this.getWidget().getParams().getWidgetId());
				this.Container.classList.add('fml-widget');
				this.Container.classList.remove('fml_popup_mode')
				this.Container.style.position = 'absolute';
				this.Container.style.width = '100%';

				while (this.getContainer().firstChild) {
					this.getContainer().removeChild(this.getContainer().firstChild);
				}

			}
			/**
			 * Loader
			 */
			{
				this.Structure.Loader = (new HtmlItem('LoaderContainer', {
					classList: ['fml-loader']
				})).addChildMultiple([
					(new HtmlItem('Preloader', {
						classList: ['fml-preloader'],
					})),
					(new HtmlItem('LoadBar', {
						classList: ['fml-loadBar'],
					})).addChild(new HtmlItem('AnimElement', {
						classList: ['fml-progress', 'fml-progress_animate']
					}))
				]);
				this.Structure.Loader.setCss({
					zIndex: '1000',
					background: 'rgb(212 212 212)',
				});
				this.getContainer().appendChild(this.Structure.Loader.getElement());

				this.Structure.FilterLoader = new HtmlItem('FilterLoader', {
					classList: ['fml-filter-loader'],
				});
				this.getContainer().appendChild(this.Structure.FilterLoader.getElement());
			}
			/**
			 * Placeholders
			 */
			{
				this.Structure.Placeholders.InputPlaceholder = new HtmlItem('InputPlaceholder', {
					classList: ['fml-widget__first-hint'],
					innerHTML: 'Если нужна доставка курьером, укажите местоположение сразу вместе с адресом',
				});
				this.Structure.Placeholders.InputPlaceholder.hide();

				this.Structure.Placeholders.PvzListPlaceholder = new HtmlItem('PvzListPlaceholder', {
					classList: ['fml-widget__sidebar-button__hint', 'fml-widget-list'],
					innerHTML: 'Список пунктов выдачи заказов',
				});

				this.Structure.Placeholders.FilterPlaceholder = new HtmlItem('FilterPlaceholder', {
					classList: ['fml-widget__sidebar-button__hint', 'fml-widget-cash'],
					innerHTML: 'Любая оплата',
				});

				this.Structure.Placeholders.FilterListPlaceholder = new HtmlItem('FilterListPlaceholder', {
					classList: ['fml-widget__sidebar-button__hint', 'fml-widget-cal'],
					innerHTML: 'Фильтр',
				});
				this.getContainer().appendChild(this.Structure.Placeholders.InputPlaceholder.getElement());
				this.getContainer().appendChild(this.Structure.Placeholders.PvzListPlaceholder.getElement());
				this.getContainer().appendChild(this.Structure.Placeholders.FilterPlaceholder.getElement());
				this.getContainer().appendChild(this.Structure.Placeholders.FilterListPlaceholder.getElement());
			}
			/**
			 * Map Container
			 */
			{
				this.Structure.MapContainer = new HtmlItem('MapBox', {
					classList: 'fml-widget__map',
				});
				const mapWidth = window.innerWidth > 960 ? 960 : window.innerWidth;
				this.getMapContainer().setCss({width: mapWidth + 'px', height: '600px'});
				this.getContainer().appendChild(this.getMapContainer().getElement());
			}
			Utility.removeStyles();
			Utility.insertAllStyles();

			this.Structure.DeliverySelector = new DeliverySelector(this.getWidget());
			this.getContainer().appendChild(this.getDeliverySelector().getElement());

			this.Structure.DeliverySidebar = new DeliverySidebar(this.getWidget());
			this.getContainer().appendChild(this.getDeliverySidebar().getElement());

			this.Structure.DeliveryPanel = new DeliveryPanel(this.getWidget());
			this.getContainer().appendChild(this.getDeliveryPanel().getElement());




			this.getDeliverySelector().close();
			this.getDeliverySidebar().close();
			this.getLoader().hide();
		};
		/** @return {HtmlItem} */
		getMapContainer = () => this.Structure.MapContainer;
		getWidget = () => this.Widget;
		/**
		 * @return {HTMLDivElement}
		 */
		getContainer = () => this.Container;
		/** @return {HtmlItem} */
		getLoader = () => this.Structure.Loader;
		/** @return {HtmlItem} */
		getFilterLoader = () => this.Structure.FilterLoader;
		getPlaceholders = () => this.Structure.Placeholders;
		/**
		 * @return {DeliverySelector}
		 */
		getDeliverySelector = () => this.Structure.DeliverySelector;
		/**
		 * @return {DeliverySidebar}
		 */
		getDeliverySidebar = () => this.Structure.DeliverySidebar;
		/**
		 * @return {DeliveryPanel}
		 */
		getDeliveryPanel = () => this.Structure.DeliveryPanel;
	}
	window.FMLogisticWidget = class FMLogisticWidget {
		/**
		 * @type {{defaultCity: string, servicepath: string, YandexMapApiKey: string, searchWhenStart: boolean, link: string, searchArea: number[][], shippingMethodsEndpointData: {weight: number}, onlyDeliveryType: string, onSelectPvzItem: params.onSelectPvzItem, onAjaxGetShippingMethods: (function(*=): *), pickPointEndpointData: {maxSize: number, maxWeight: number}, onAjaxGetPickPoints: (function(*=): *), onSelectCourierItem: params.onSelectCourierItem}}
		 */
		constructor(params) {
			let self = this;
			self.Params = new ParamsCollections(params, self);
			self.DataCollection = new DataCollection();
			self.AjaxEngine = new AjaxEngine(self);
			self.HtmlContainer = new HtmlContainer(self);
			self.MapCover = new MapCover(self);
			let Loader = new StateLoader(self, self.getMap());
			Loader.load(self.ready.bind(self));
			self.ClosedPopup = false;
			self.coordsCenter = [];
		};

		ready = () => {
			if (window.FMLogisticWidgetMaps && window.FMLogisticWidgetMaps[this.getParams().getWidgetId()]) {
				window.FMLogisticWidgetMaps[this.getParams().getWidgetId()].destroy();
			}
			this.getContainer().createStructure();

			if (this.getParams().isOnlyPvz() !== true) {
				this.getContainer().getDeliverySidebar().getButtonBuck().handleEvent('onclick', (e) => {
					e.preventDefault();
					this.getContainer().getDeliverySidebar().close();
					this.getContainer().getDeliverySelector().open();
					this.getMap().recenterMap(this.GlobalZoom).then();
				});
			}
			if (this.getParams().isCourierOnly() !== true) {
				this.getContainer().getDeliverySelector().getPvzVariant().handleEvent('onclick', e => {
					e.preventDefault();
					this.getContainer().getLoader().show();
					setTimeout(() => {
						this.openPvzAction().then( r => {
							this.getMap().resetCenter().then();
						});
					}, 100)
				});
				/**
				 * @type {HtmlItem}
				 */
				let Panel = this.getContainer().getDeliveryPanel().getHtmlItem();
				let PvzList = this.getContainer().getDeliveryPanel().getHtmlItem().getChild('Containers').getChild('PvzListContainer');
				let FilterList = this.getContainer().getDeliveryPanel().getHtmlItem().getChild('Containers').getChild('FilterContainer');
				let PvzButton = this.getContainer().getDeliverySidebar().getButtonPoint();
				let FilterButton = this.getContainer().getDeliverySidebar().getButtonFilter();
				let DetailFrame = this.getContainer().getDeliveryPanel().getDetail().getHtmlItem();
				let PanelBuckButton = this.getContainer().getDeliveryPanel().getHtmlItem().getChild('Containers').getChild('Buck');

				const removeSelectWhen = async () => {
					PvzButton.removeCurrent();
					FilterButton.removeCurrent();
					FilterList.hide();
					PvzList.hide();
					Panel.removeOpen();
					this.getContainer().getDeliveryPanel().getHtmlItem().getChild('Containers').removeCurrent();
					DetailFrame.removeCurrent();
					await this.getMap().getMap().setZoom(this.GlobalZoom);
					await this.getMap().resetCenter();
				};
				PvzButton.handleEvent('onclick', (e) => {
					const hasSelect = PvzButton.getElement().classList.contains('current');
					removeSelectWhen().then(e => {
						if (hasSelect) {
							return;
						}
						this.getContainer().getDeliveryPanel().getPvzContainer().show();
						PvzList.show();
						PvzButton.setCurrent();
						Panel.setOpen();
						this.getMap().recenterMap(this.GlobalZoom).then();
						if (window.innerWidth <= 767) {
							Sidebar.close();
						}
					})

				});
				FilterButton.handleEvent('onclick', () => {
					const hasSelect = FilterButton.getElement().classList.contains('current');
					removeSelectWhen().then(e => {
						if (hasSelect) {
							return;
						}
						this.getContainer().getDeliveryPanel().getFilterContainer().show();
						FilterList.show();
						FilterButton.setCurrent();
						Panel.setOpen();
						this.getMap().recenterMap(this.GlobalZoom).then();
						if (window.innerWidth <= 767) {
							Sidebar.close();
						}
					})

				});
				PanelBuckButton.handleEvent('onclick', e => {
					removeSelectWhen().then(e => {
						Sidebar.open();

					});
				})
				this.getContainer().getMapContainer().addEventListener('click', (e) => {
					const isOpenFilter = FilterButton.getElement().classList.contains('current');
					if (isOpenFilter) {
						removeSelectWhen().then();
					}
				});

				/**
				 * Bind variant type select
			 	*/
				let Sidebar = this.getContainer().getDeliverySidebar();
				let Payment = Sidebar.getButtonPayment();

				Payment.setData({
					active: false,
					filterBy: 'all'
				})
				let allSvg = Payment.getElement().childNodes[2];
				let cacheSvg = Payment.getElement().childNodes[1];
				let cardSvg = Payment.getElement().childNodes[0];


				Payment.handleEvent('onclick', () => {
					const active = Payment.getData().active;
					if (!active) {
						Payment.getData().active = true;
						Payment.getData().filterBy = 'cache';
					} else if (active && Payment.getData().filterBy === 'cache') {
						Payment.getData().filterBy = 'card';
					} else {
						Payment.getData().filterBy = 'all';
						Payment.getData().active = false;
					}
					if (Payment.getData().active) {
						Payment.setCurrent();
					} else {
						Payment.removeCurrent();
					}
					switch (Payment.getData().filterBy) {
						case 'all':
							cardSvg.style.display = 'none';
							cacheSvg.style.display = 'none';
							allSvg.style.display = 'block';
							break;
						case 'card':
							cacheSvg.style.display = 'none';
							allSvg.style.display = 'none';
							cardSvg.style.display = 'block';
							break;
						case 'cache':
							cardSvg.style.display = 'none';
							allSvg.style.display = 'none';
							cacheSvg.style.display = 'block';
							break;
					}
					this.getContainer().getDeliverySidebar().setPanelInfoPayTitle(Payment.getData().filterBy);
					this.filterAction('pay', true, Payment.getData().filterBy).then();
				});
			}
			this.renderMap().then();

			let PopupModeButton = null;
			if (this.getParams().isPopupMode()) {
				this.getContainer().getContainer().classList.add('fml_popup_mode');
				PopupModeButton = new HtmlItem('PopupModeButton', {
					classList: ['fml_popup_mode_close'],
				});
				this.getContainer().getContainer().appendChild(PopupModeButton.getElement());
				PopupModeButton.handleEvent('onclick', (e) => {
					this.hide();
				});
			} else {
				this.getContainer().getContainer().classList.remove('fml_popup_mode');
			}

		};
		renderMap = async () => {
			this.getContainer().getLoader().show();
			let options = {
				provider: 'yandex#map',
				size: 'large',
			};
			if (this.getParams().hasSearchArea()) {
				options.strictBounds = true;
				options.boundedBy = this.getParams().getSearchArea();
			}
			this.getMap().createSearchInput(options);

			let location;
			if (!this.getParams().getDefaultCity()) {
				location = await FmlYmapNamespace.geolocation.get();
			} else {
				location = await FmlYmapNamespace.geocode(this.getParams().getDefaultCity(), {
					results: 1
				});
			}
			/**
			 * @type {{geometry : {getCoordinates : function}}}
			 */
			let firstGeoObject = location.geoObjects.get(0);
			let coords = firstGeoObject.geometry.getCoordinates();
			this.getMap().setMap(new FmlYmapNamespace.Map(this.getContainer().getMapContainer().getElement(), {
				zoom: 12,
				controls: [],
				center: coords,
				duration: 300,
			}));
			this.getMap().getMap().controls.add(new FmlYmapNamespace.control.ZoomControl(), {
				position: {
					left: 12,
					bottom: 70
				}
			});
			this.resetCenter();

			this.getMap().getMap().controls.add(this.getMap().getSearchInput());
			if (!this.getParams().searchOnStat()) {
				this.getContainer().getLoader().hide();
			}
			let SearchInput;
			const PlaceholderShowHide = (e) => {
				if (SearchInput.value === '') {
					this.getContainer().getPlaceholders().getForInput().show();
				} else {
					this.getContainer().getPlaceholders().getForInput().hide();
				}
			}
			let layoutReady = false;
			while (layoutReady === false) {
				await new Promise(r => setTimeout(r, 100));
				if (this.getMap().getSearchInput()._layout) {
					layoutReady = true;
					SearchInput = this.getMap().getSearchInput()._layout._parentElement.querySelector('input');
					let searchString = this.getParams().getDefaultCity() ? this.getParams().getDefaultCity() : '';
					SearchInput.value = searchString;
					PlaceholderShowHide();
					this.getMap().getSearchInput()._provider.state._data.request = this.getParams().getDefaultCity();
					if (searchString) {
						const geocodeResult = await FmlYmapNamespace.geocode(searchString, {
							json: true,
							results: 1
						});
						let members = geocodeResult.GeoObjectCollection.featureMember;
						let geoObjectData = (members && members.length) ? members[0].GeoObject : null;
						if (geoObjectData) {
							geoObjectData.description;
							let Search = geoObjectData.metaDataProperty.GeocoderMetaData.text;
							if (geoObjectData.metaDataProperty.GeocoderMetaData.kind === 'street') {
								Search = Search.replace(', ' + geoObjectData.name, '')
							}
							await this.shippingMethodsAction(Search);
						}
					}

					this.getMap().getSearchInput().events.add('clear', PlaceholderShowHide);
					SearchInput.oninput = PlaceholderShowHide;
					SearchInput.change = PlaceholderShowHide;
				}
			}

			this.getMap().getSearchInput().events.add('resultshow', (e) => {
				this.shippingMethodsAction(this.getMap().getSearchInput().getRequestString()).then(e => {
					this.resetCenter();
					this.GlobalZoom = this.getMap().getMap().getZoom();
					this.getMap().recenterMap(this.GlobalZoom).then();
				});
			});
			this.getMap().removeBalloon();
		};
		shippingMethodsAction = async (searchString) => {
			this.getContainer().getLoader().show();
			this.getMap().getClusterer().removeAll();
			let ShippingMethodsData;
			if (this.getData().hasShippingMethods(searchString)) {
				ShippingMethodsData = this.getData().getShippingMethods(searchString);
			} else {
				ShippingMethodsData = await this.getAjaxEngine().getShippingMethods(searchString);
				if (this.getParams().hasOnAjaxGetShippingMethods()) {
					this.getParams().getOnAjaxGetShippingMethods()(ShippingMethodsData);
				}
				this.getData().setShippingMethods(searchString, ShippingMethodsData);
			}
			if (!ShippingMethodsData.address) {
				this.getContainer().getLoader().hide();
				alert(JSON.stringify(ShippingMethodsData, null, 4))
				return;
			}
			this.getContainer().getDeliverySidebar().close();
			this.getContainer().getDeliveryPanel().close(false);
			this.getData().setPostcode(ShippingMethodsData.address.postcode);
			this.getData().setSelectedCity(ShippingMethodsData.address.city);
			this.getData().resetShippingMethodsByCode();
			this.getContainer().getDeliverySelector().removeCourierItems();
			this.getData().resetCourierItems();
			this.getData().resetPvzItems();

			let CourierVariants = [];
			let PvzVariants = [];

			for (let key in ShippingMethodsData.shipping_methods) {
				let deliveryVariant = ShippingMethodsData.shipping_methods[key];
				const dayMin = deliveryVariant.days_min > 0 || deliveryVariant.days_min === 0
					? deliveryVariant.days_min : null;
				const price = deliveryVariant.price > 0 || deliveryVariant.price === 0
					? deliveryVariant.price : null;
				const contractorCode = deliveryVariant.contractor_code;
				this.getData().addShippingMethodByCode(contractorCode, {
					name: deliveryVariant.name,
					Id: deliveryVariant.contractor_id,
					code: contractorCode,
					price: price,
					days_min: dayMin,
					dayType: deliveryVariant.days_type,
					logo_small: deliveryVariant.logo_small,
					count: 0,
				});
				if (deliveryVariant.delivery_type === 'courier') {
					let CourierDeliveryVariant = new DeliveryCourierVariantItem(deliveryVariant, this);
					this.getData().addCourierItem(CourierDeliveryVariant);
					this.getContainer().getDeliverySelector().addCourierItem(CourierDeliveryVariant)
					CourierVariants.push({
						price: price,
						date: dayMin,
					});
				} else {
					PvzVariants.push({
						price: price,
						date: dayMin,
					});
				}
			}
			if (this.getParams().isCourierOnly() !== true) {
				if (PvzVariants.length > 0) {
					this.getContainer().getDeliverySelector().setPvzTypeVariantData(
						this.getData().calkMinDatePrice(PvzVariants)
					);
					this.getContainer().getDeliverySelector().getPvzVariant().show();
				} else {
					this.getContainer().getDeliverySelector().getPvzVariant().hide();
				}
			}
			if (this.getParams().isOnlyPvz() !== true) {
				if (CourierVariants.length > 0) {
					this.getContainer().getDeliverySelector().setCourierTypeVariantData(
						this.getData().calkMinDatePrice(CourierVariants)
					);
					this.getContainer().getDeliverySelector().getCourierVariant().show();
				} else {
					this.getContainer().getDeliverySelector().getCourierVariant().hide();
				}
			}
			if (this.getParams().isOnlyPvz() === true) {
				await this.openPvzAction();
				return;
			}
			this.getContainer().getDeliverySelector().open();
			this.getContainer().getLoader().hide();
			this.getMap().removeBalloon();
		}
		openPvzAction = async () => {
			this.getContainer().getLoader().show();
			this.getContainer().getDeliverySelector().close();
			this.getContainer().getDeliveryPanel().getPvzContainer().removeChilds();
			this.getContainer().getDeliveryPanel().getFilterContainer().removeChilds();

			this.getData().resetPvzItems();
			this.getData().resetFilterItems();
			this.getMap().getClusterer().removeAll();

			let PvzList;
			if (this.getData().hasPvzList() === false) {
				PvzList = await this.getAjaxEngine().getPvzList();
				if (this.getParams().hasOnAjaxGetPickPoints()) {
					this.getParams().getOnAjaxGetPickPoints()(PvzList);
				}
				this.getData().addPvzList(PvzList);
			} else {
				PvzList = this.getData().getPvzList();
			}
			let countPvz = 0;

			let existCoords = [];

			for (let i in PvzList.pointsList) {
				let Point = PvzList.pointsList[i];

				const coords = Point.gpsCoordinates.replace(' ', '').split(',');
				let lat = Number(coords[0]);
				let lon = Number(coords[1]);

				if (existCoords.includes(Point.gpsCoordinates)) {
					//lat = ((lat * 1000000) + 40)/1000000;
					lon = ((lon * 1000000) + 40)/1000000;
					Point.gpsCoordinates = lat + ', ' + lon;
				}
				existCoords.push(Point.gpsCoordinates);

				Point.coords = [lat, lon];

				let Method = this.getData().getShippingMethodByCode(Point.lms);
				if (Method) {
					Point.Method = Method;
					let PvzItem = new DeliveryPvzItem(Point, this);
					let PlaceMark = this.getMap().createPlaceMark(Point, () => {
						this.getContainer().getDeliveryPanel().openDetail(PvzItem);
					});
					PvzItem.setPlaceMark(PlaceMark);
					this.getMap().getClusterer().add(PlaceMark);
					PvzItem.getHtmlItem().handleEvent('onclick', e => {
						this.getContainer().getDeliveryPanel().openDetail(PvzItem);
					});
					this.getContainer().getDeliveryPanel().addPvzPoint(PvzItem);
					if (this.getData().getShippingMethodByCode(PvzItem.getData().lms))
						this.getData().getShippingMethodByCode(PvzItem.getData().lms).count++;
					this.getData().addPvzItem(PvzItem);
					countPvz++;
				}
			}
			let ShippingMethods = this.getData().getAllShippingMethodByCode();

			for (let i in ShippingMethods) {
				let Method = ShippingMethods[i];
				if (Method.count > 0) {
					let FilterItem = new DeliveryPvzFilterItem(ShippingMethods[i], this);
					this.getContainer().getDeliveryPanel().addFilterItem(FilterItem);
					this.getData().addFilterItem(FilterItem);
					FilterItem.getData().active = true;
					FilterItem.getHtmlItem().handleEvent('onclick', () => {
						const isSelected = FilterItem.getData().active;
						if (isSelected) {
							FilterItem.getData().active = false;
							FilterItem.getElement().classList.add('fml-widget__disabled');
							this.filterAction('filter', true, FilterItem.getData().code).then();
						} else {
							FilterItem.getData().active = true;
							FilterItem.getElement().classList.remove('fml-widget__disabled');
							this.filterAction('filter', false, FilterItem.getData().code).then();
						}
					})
				}
			}
			this.getContainer().getDeliveryPanel().setPointTitleCount(countPvz);
			this.getContainer().getLoader().hide();
			this.getContainer().getDeliverySidebar().open();
			this.getMap().removeBalloon();
		}
		filterAction = async (by, action, code) => {
			const listAndPlaceMarkAction = (PvzItem) => {
				PvzItem.getData().isVisible = PvzItem.getData().isVisibleByPay === true
					&& PvzItem.getData().isVisibleByType === true
					&& PvzItem.getData().isVisibleByOperator === true;
				if (PvzItem.getData().isVisible === true && PvzItem.getData().wasVisible === false) {
					PvzItem.getHtmlItem().setCss({display: 'flex'});
					this.getMap().getClusterer().add(PvzItem.getPlaceMark());
					PvzItem.getPlaceMark().options.set('visible', true);
				}
				if (PvzItem.getData().isVisible === false && PvzItem.getData().wasVisible === true) {
					PvzItem.getHtmlItem().hide();
					PvzItem.getPlaceMark().options.set('visible', false);
					this.getMap().getClusterer().remove(PvzItem.getPlaceMark());
				}
			};

			const filterByFunction = async (filterCallback) => {
				let FilterLoader = this.getContainer().getFilterLoader().getElement();
				FilterLoader.style.width = '0%';

				let countPvz = 0;
				let Items = this.getData().getPvzItems();
				const keys = Object.keys(Items);
				for (let i in Items) {
					let PvzItem = Items[i];
					PvzItem.getData().wasVisible = PvzItem.getData().isVisible;
					await filterCallback(PvzItem);
					if (PvzItem.getData().isVisible) {
						countPvz++;
					}
					let percent = Math.round((keys.indexOf(i) / keys.length) * 100);
					FilterLoader.style.width =  percent + '%';
				}
				FilterLoader.style.width = '0%';
				this.getContainer().getDeliveryPanel().setPointTitleCount(countPvz);
			};
			switch (by) {
				case 'filter':
					let ActiveFilterIcon = this.getContainer().getDeliverySidebar().getButtonFilter().getChild('IconActiveContainer').getElement();
					let IsActiveFilter = false;
					await filterByFunction(async (ItemPvz) => {
						const isFiltered = ItemPvz.getData().lms === code;
						if (!isFiltered) {
							IsActiveFilter = ItemPvz.getData().isVisibleByOperator === false ? true : IsActiveFilter;
							return;
						}
						ItemPvz.getData().isVisibleByOperator = !action;
						IsActiveFilter = ItemPvz.getData().isVisibleByOperator === false ? true : IsActiveFilter;
						listAndPlaceMarkAction(ItemPvz);
					})
					if (IsActiveFilter) {
						ActiveFilterIcon.classList.add('current');
					} else {
						ActiveFilterIcon.classList.remove('current');
					}
					break;
				case 'pay':
					await filterByFunction(async (item) => {
						switch (code) {
							case 'all':
								item.getData().isVisibleByPay = true;
								break;
							case 'card':
								item.getData().isVisibleByPay = item.getData().cardPayment === 1;
								break;
							case 'cache':
								item.getData().isVisibleByPay = item.getData().cashPayment === 1;
								break;
						}
						listAndPlaceMarkAction(item);
					});
					break;
				// case 'type':
				// 	await filterByFunction(async (item) => {
				// 		switch (code) {
				// 			case 'all':
				// 				item.getData().isVisibleByType = true;
				// 				break;
				// 			case 'terminal':
				// 				item.getData().isVisibleByType = item.getData()['point-type'] === '1';
				// 				break;
				// 			case 'pvz':
				// 				item.getData().isVisibleByType = item.getData()['point-type'] === '2';
				// 				break;
				// 		}
				// 		listAndPlaceMarkAction(item);
				// 	});
				// 	break;
			}
		}
		destroy = () => {
			this.getMap().getMap().destroy();
			while (this.getContainer().getContainer().firstChild) {
				this.getContainer().getContainer().removeChild(this.getContainer().getContainer().firstChild);
			}
			Utility.removeStyles();
		}
		reinitialize = (params) => {
			this.destroy();
			this.Params = new ParamsCollections(params, this);
			this.ready();
		}
		show = () => {
			this.getContainer().getContainer().style.display = 'flex';
			this.ClosedPopup = false;
		};
		hide = () => {
			this.ClosedPopup = true;
			this.getContainer().getContainer().style.display = 'none';
			if (this.getParams().hasHandlerOnPopupClose()) {
				this.getParams().getOnPopupCloseHandler()(this);
			}
		};
		isClosedPopup = () => this.ClosedPopup;
		getData = () => this.DataCollection;
		getAjaxEngine = () => this.AjaxEngine;
		getMap = () => this.MapCover;
		getContainer = () => this.HtmlContainer;
		getParams = () => this.Params;
		getCenter = () => this.coordsCenter;
		resetCenter = () => this.coordsCenter = this.getMap().getMap().getCenter();
		makeLogoParams = logoSrc => {
			let logoParams = {
				name: 'logo',
				tagName: 'img',
				src: logoSrc,
				alt: 'logo'
			};
			if (!logoSrc) {
				logoParams = {
					name: 'Logo',
					classList: ['fml-widget__panel-content__default_logo'],
					alt: 'logo'
				};
			}
			return new HtmlItem('Logo', logoParams);
		}
		isMobile = () => {
			const toMatch = [
				/Android/i,
				/webOS/i,
				/iPhone/i,
				/iPad/i,
				/iPod/i,
				/BlackBerry/i,
				/Windows Phone/i
			];
			return toMatch.some((toMatchItem) => {
				return navigator.userAgent.match(toMatchItem);
			});
		}
	};
})(window)

















