<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример работы виджета FML</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <script type="text/javascript" src="release/fmlogisticwidget.min.js?<?echo time()?>"></script>
</head>
<body>
<script type="text/javascript">
    new FMLogisticWidget({
        service_path: 'service.php',
        //default_city: 'Россия, Москва', // Строка выбранная из яндека
        link: 'for_fmlogisticwidget',
        //searchArea: [[54.496848063378806, 55.29955631249997],[54.97205624926723, 56.617915687500016]], // Поиск только по заданной области (Квадрат области по координатам в яндекс картах)
        //какойто документации на него нет у яндекса параметры  strictBounds = true и boundedBy этому массиву https://yandex.ru/dev/maps/jsapi/doc/2.1/ref/reference/control.SearchControl.html#control.SearchControl__param-parameters.options.strictBounds
        //проще всего получить создать карту распределить по области ограничения и из переменнй карты вызвать метод .getBounds()
        //only_delivery_type: 'Courier', //Pvz или Courier // Только выбранный тип доставки
        //only_info: true,
        //popup_mode: true,
        cargo: {
            weight: 10,
            max_weight : 3,
            max_size : 10,
        },
        yandex_map_api_key: '76211b49-d943-4b54-bc7a-86949d716e19',
        onSelectCourierItem: function (ItemCourier) {
            console.log(ItemCourier.getData())
        },
        onSelectPvzItem: function (ItemPvz) {
            console.log(ItemPvz.getData())
        },
        onAjaxGetShippingMethods: function (Result) {
            console.log(Result)
        },
        onAjaxGetPickPoints: function (Result) {
            console.log(Result);
        }
    });
</script>
<div id="for_fmlogisticwidget"></div>
</body>
</html>
