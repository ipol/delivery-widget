<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример встраивания виджета Catapulto в виде всплывающего окна</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <script type="text/javascript" src="release/fmlogisticwidget.min.js?<?echo time()?>"></script>
</head>
<body>
<div class="body-blackout"></div>
<h2>Demo пример встраивания виджета FM Logistic в виде всплывающего окна</h2>
<button type="button" class="btn popup-trigger" data-popup-trigger="one">Открыть
    виджет
</button>
<div>
    <p>Были выбраны следующие данные:</p>
    <div id="selectedInfo"></div>
</div>

<div class="popup-modal" data-popup-modal="one">
    <div id="for_fmlogisticwidget"></div>
</div>

<script type="text/javascript">
    const modalTriggers = document.querySelector('.popup-trigger');
    const { popupTrigger } = modalTriggers.dataset;
    const popupModal = document.querySelector(`[data-popup-modal="${popupTrigger}"]`);
    const bodyBlackout = document.querySelector('.body-blackout');
    let WidgetObject = false;
    function CloseWidget() {
        popupModal.classList.remove('is--visible');
        bodyBlackout.classList.remove('is-blacked-out');
    }
    modalTriggers.addEventListener('click', () => {
        popupModal.classList.add('is--visible');
        bodyBlackout.classList.add('is-blacked-out');
        bodyBlackout.addEventListener('click', CloseWidget);
        if (WidgetObject === false) {
            WidgetObject = new FMLogisticWidget({
                service_path: 'service.php',
                link: 'for_fmlogisticwidget',
                cargo: {
                    weight: 10,
                    max_weight : 3,
                    max_size : 10,
                },
                yandex_map_api_key: '76211b49-d943-4b54-bc7a-86949d716e19',
                popup_mode: true,
                onPopupClose: (Widget) => {
                    console.log(Widget.isClosedPopup());
                    CloseWidget();
                },
                onSelectPvzItem: SelectWidgetData,
                onSelectCourierItem: SelectWidgetData,
            });
        } else {
            WidgetObject.show();
        }
    });
    function SelectWidgetData(item, widget) {
        let data = item.getData();
        if (data.PlaceMark) {
            data = Object.assign({}, data);
            delete data.PlaceMark;
        }
        document.querySelector('#selectedInfo').innerHTML = '<pre>' + JSON.stringify( data, null, 4) + '</pre>';
        CloseWidget();
    }
</script>
<style>
    @media only screen and (max-width: 989px) {
        div.popup-modal {
            left: 5px;
            height: auto;
            padding: 0;
            width: calc(100% - 20px);
        }
    }
    .body-blackout {
        position: absolute;
        z-index: 1010;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .65);
        display: none;
    }
    .body-blackout.is-blacked-out {
        display: block;
    }
    .btn {
        height: 36px;
        color: #fff;
        font-family: Roboto,sans-serif;
        font-weight: 400;
        font-size: 18px;
        line-height: 1.17;
        border: 1px solid #01bd6c;
        background: #01bd6c;
        cursor: pointer;
        padding: 0 15px;
        transition: all 0.15s;
    }

    .btn:hover {
        color: #01bd6c;
        background: #ffffff;
    }
    .popup-trigger {
        display: inline-block;
    }
    .popup-modal {
        height: min-content;
        max-width: 1050px;
        width: 100%;
        background-color: #fff;
        position: fixed;
        padding: 0;
        opacity: 0;
        pointer-events: none;
        transition: all 300ms ease-in-out;
        z-index: 1011;
        left: calc(50% - 40vh);
        top: 10%;
    }
    @media screen and (max-width: 1090px){
        div.popup-modal {
            left: calc(8% - 4vh);
        }
    }
    @media screen and (max-width: 767px){
        div.popup-modal {
            left: 5px;
            height: auto;
            padding: 0;
            width: calc(100% - 10px);
        }
    }
    .popup-modal.is--visible {
        opacity: 1;
        pointer-events: auto;
    }
    .popup-modal__close svg path {
        transition: all 0.15s;
    }
    .popup-modal__close:hover svg path {
        fill: #000000;
        opacity: 1;
    }
    .popup-modal__close span {
        font-size: 2rem;
        transform: rotate(45deg);
    }
    @media only screen and (max-width: 767px) {
        .popup-modal {
            height: 100vh;
            width: 100vw;
            display: flex;
            padding: 45px 0 0;
        }
    }
</style>
</body>
</html>
